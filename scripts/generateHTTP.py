import random

#file = open("../Server/httpClientTests/tests4-BIG.http",'w') 

#-----------------------VARIABLES-----------------------------------------
file = open("../grantapp/populateAppSmall.http",'w') 
institutionNumber = 3
studentNumberPerInstitution = 10
reviewersNumberPerInstitution = 2
sponsorsNumber = 5
grantcallNumberPerSponsor = 2
grantapplicationNumberPerGrantCall = 5

#-----------------------INSTITUTIONS----------------------------------------- (1-10)
inst = '###\nPOST http://localhost:8080/institutions\nAuthorization: Basic admin admin\nContent-Type: application/json\n\n{{"id": {0},"name": "institution_{0}","address": "institution_{0}","contact": "institution_{0}","email": "institution_{0}"}}\n\n'
institutionMinID = 1
institutionMaxID = institutionMinID + institutionNumber -1 
institutionID = 1
for institutionID in range(1, institutionNumber+1):
	file.write(inst.format(institutionID)) 

print("rinstitutionsIDS:" + str(institutionMinID) + "-" + str(institutionMaxID))

#-----------------------REVIEWERS----------------------------------------- (11-110)
revi = '###\nPOST http://localhost:8080/reviewers/\nAuthorization: Basic admin admin\nContent-Type: application/json\n\n{{  "id": {0},  "userName": "reviewer_{0}_{1}",  "name": "reviewer_{0}_{1}","password": "reviewer_{0}_{1}",  "email": "reviewer_{0}_{1}",  "birthDate": "2020-10-21T20:50:08.648Z",  "institution": {1}}}\n\n'
reviewerMinID = institutionMaxID + 1
reviewerMaxID = reviewerMinID + reviewersNumberPerInstitution * institutionNumber -1
institutionID = 1
reviewerID=0
for institutionID in range(1, institutionNumber+1):
	for reviewerID in range(reviewersNumberPerInstitution):
		file.write(revi.format(reviewerID, institutionID))
	reviewerID=0	

print("reviewersIDS:" + str(reviewerMinID) + "-" + str(reviewerMaxID) )


#-----------------------SPONSORS----------------------------------------- (111-135)
spon = '###\nPOST http://localhost:8080/sponsors\nAuthorization: Basic admin admin\nContent-Type: application/json\n\n{{ "id": {0}, "userName": "sponsor_{0}", "name": "sponsor_{0}", "email": "sponsor_{0}",  "password": "sponsor_{0}",  "phoneNumber": "sponsor_{0}"}}\n\n'
sponsorsMinID = reviewerMaxID + 1
sponsorsMaxID = sponsorsMinID + sponsorsNumber -1
for sponsorID in range(sponsorsNumber):
	file.write(spon.format(sponsorID))

print("sponsorsIDS:" + str(sponsorsMinID) + "-" + str(sponsorsMaxID) )

#-----------------------STUDENTS----------------------------------------- (136-335)
stud = '###\nPOST http://localhost:8080/students\nAuthorization: Basic admin admin\nContent-Type: application/json\n\n{{  "id": {0},  "userName": "student_{0}_{1}",  "name": "student_{0}_{1}",  "password": "student_{0}_{1}",  "email": "student_{0}_{1}",  "birthDate": "2020-10-21T20:50:08.648Z",  "institution": {1},  "average": 20,  "cv": {0}}}\n\n'
studentMinID = sponsorsMaxID + 1
studentMaxID = studentMinID + studentNumberPerInstitution * institutionNumber * 2 -1
institutionID = 0
for institutionID in range(1, institutionNumber+1):
	for studentID in range(studentNumberPerInstitution):
		file.write(stud.format(studentID, institutionID)) 
	studentID = 0

print("studentsIDS:" + str(studentMinID) + "-" + str(studentMaxID) )

#-----------------------CREATE GRANT CALLS-----------------------------------------(336-535)
grantcall = '###\nPOST http://localhost:8080/grant_calls/\nAuthorization: Basic admin admin\nContent-Type: application/json\n\n\n{{"dataItems": [{{ "header":"Avarage","mandatory": true,"subTitle": "Give me your avarage","type":"string"}},{{"header": "Age","mandatory":false,"subTitle": "Give me your age", "type": "int"}}],  "description": "GrantCall_{0}", "evaluationPanel":{{ "panelChair": {1}, "reviewersIDs": [{1}, {2}]}},"expireDate": "2020-10-25T12:37:11.502Z",  "funding": 1000,  "sponsorId": {3},  "startDate": "2020-10-25T12:37:11.503Z",  "title": "GrantCall_{0}"}}\n\n'
grantcallMinID = studentMaxID + 1
grantcallMaxID = grantcallMinID + grantcallNumberPerSponsor * sponsorsNumber * 4 -1

grantCall_Reviewer={}
grantcallIDddd = grantcallMinID + 1

for sponsorID in range(sponsorsMinID, sponsorsMaxID+1):
	for grantcallID in range(grantcallNumberPerSponsor):
		panelChair = random.randint(reviewerMinID, reviewerMinID+int(reviewerMaxID/2))
		reviewr = random.randint(reviewerMinID+int(reviewerMaxID/2)+1, reviewerMaxID)
		file.write(grantcall.format(
			str(grantcallID) + "_" +str(sponsorID), 
			panelChair, 
			reviewr,
			sponsorID))
		grantCall_Reviewer[grantcallIDddd] = (panelChair, reviewr)
		grantcallIDddd+=4
	grantcallID=0

print("grantCallIDS:" + str(grantcallMinID) + "-" + str(grantcallMaxID) )
#print(grantCall_Reviewer)

#-----------------------GRANT APPLCATIONS-----------------------------------------(536-635)
grantapplication = '###\nPOST http://localhost:8080/grant_applications\nAuthorization: Basic admin admin\nContent-Type: application/json\n\n{{  "fieldsResponses": [    {{      "dataItemId": {0},      "grantAppId": 0,      "id": 0,      "value": "string"    }}  ],  "grantCallId": {1},  "id": 0,  "status": "IN_VOTING",  "studentID": {2}}}\n\n'
grantappMinID = grantcallMaxID + 1
grantappMaxID = grantappMinID +  grantapplicationNumberPerGrantCall * (sponsorsNumber * grantcallNumberPerSponsor) -1
studentID = studentMinID+1

GrantCall_grantApp={}
grantAppIDdddd = grantappMinID+1

for grantCallID in range(grantcallMinID+1, grantcallMaxID, 4):
	dateItemID = grantCallID + 1
	GrantCall_grantApp[grantCallID] = []
	for grantAppID in range(grantapplicationNumberPerGrantCall):
		file.write(grantapplication.format(dateItemID,grantCallID, studentID))
		studentID += 2
		GrantCall_grantApp[grantCallID].append(grantAppIDdddd)
		grantAppIDdddd+=2

print("grantAppIDS:" + str(grantappMinID) + "-" + str(grantappMaxID) )
#print(GrantCall_grantApp)

#-----------------------REVIEWS-----------------------------------------(636-935)
review = '###\nPOST http://localhost:8080/reviews/\nAuthorization: Basic admin admin\nContent-Type: application/json\n\n{{  "id": 0,  "note": {0},  "description": "Excellent",  "reviewerID": {1},  "grantApplicationId": {2}}}\n\n'
reviewMinID = grantappMaxID + 1
grantapplicationNumber = grantapplicationNumberPerGrantCall * grantcallNumberPerSponsor * sponsorsNumber
reviewMaxID = reviewMinID +  grantapplicationNumber * 3 -1

for gc in GrantCall_grantApp:
	for ga in GrantCall_grantApp[gc]:
		panelChairReviewerID = grantCall_Reviewer[gc][0] 
		reviewerID = grantCall_Reviewer[gc][1]
		note = random.randint(1, 20)
		file.write(review.format(note, panelChairReviewerID, ga))
		file.write(review.format(note, reviewerID, ga))

print("reviewsIDS:" + str(reviewMinID) + "-" + str(reviewMaxID) )

#----------------------------------------------------------------


file.close() 