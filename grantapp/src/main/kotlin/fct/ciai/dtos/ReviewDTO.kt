package fct.ciai.dtos

import fct.ciai.daos.ReviewDAO

class ReviewDTO(
        val id: Long,
        val note: Int,
        val description: String,
        val reviewerID: Long,
        val grantApplicationId: Long
) {
    constructor(review: ReviewDAO): this(
            review.id,
            review.note,
            review.description,
            review.reviewer.id,
            review.grantApplication.id)

    constructor():this(0,0,"",0,0)

    override fun equals(other: Any?)
            = (other is ReviewDTO)
            && id == other.id
            && note == other.note
            && description == other.description
            && reviewerID == other.reviewerID
            && grantApplicationId == other.grantApplicationId
}
