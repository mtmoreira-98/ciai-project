package fct.ciai.dtos

import fct.ciai.daos.InstitutionDAO

class InstitutionDTO(
        val id: Long,
        val name: String,
        val address: String,
        val contact: String,
        val email: String)
{
    constructor() : this(0, "", "", "","")
    constructor(it: InstitutionDAO): this(it.id, it.name, it.address, it.contact, it.email)

    // Only need for test purposes
    override fun equals(other: Any?)
            = (other is InstitutionDTO)
            && id == other.id
            && name == other.name
            && address == other.address
            && contact == other.contact
            && email == other.email
}




