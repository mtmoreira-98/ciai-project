package fct.ciai.dtos

import fct.ciai.daos.ReviewDAO

class FinalReviewDTO(
        val id: Long,
        val note: Int,
        val description: String,
        val reviewerID: Long,
        val grantApplicationId: Long,
        val accepted: Boolean
) {
    constructor():this(0,0,"",0,0, false)

    override fun equals(other: Any?)
            = (other is FinalReviewDTO)
            && id == other.id
            && note == other.note
            && description == other.description
            && reviewerID == other.reviewerID
            && grantApplicationId == other.grantApplicationId
}
