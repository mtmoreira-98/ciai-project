package fct.ciai.dtos.accounts.sponsors

import fct.ciai.daos.SponsorDAO
import fct.ciai.dtos.accounts.AccountDTO

class SponsorDTO(
        val id: Long,
        val userName: String,
        val name: String,
        val email: String,
        val password: String,
        val phoneNumber: String
) {

    constructor() : this(0, "", "", "", "", "")

    constructor(sponsorDAO: SponsorDAO) : this(
            sponsorDAO.id,
            sponsorDAO.userName,
            sponsorDAO.name,
            sponsorDAO.email,
            sponsorDAO.password,
            sponsorDAO.phoneNumber
    )

    // Only need for test purposes
    override fun equals(other: Any?): Boolean {
        return (other is SponsorDTO)
                && this.id == other.id
                && this.userName == other.userName
                && this.name == other.name
                && this.email == other.email
                && this.phoneNumber == other.phoneNumber
    }

}
