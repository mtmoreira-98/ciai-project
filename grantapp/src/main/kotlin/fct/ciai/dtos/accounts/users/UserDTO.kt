package fct.ciai.dtos.accounts.users

import com.fasterxml.jackson.annotation.JsonFormat
import fct.ciai.daos.UserDAO
import fct.ciai.dtos.accounts.AccountDTO
import java.util.*

open class UserDTO(
        val id: Long,
        val userName: String,
        val name: String,
        val password: String,
        val email: String,
        @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
        val birthDate: Date,
        val institution: Long
){
        constructor(user: UserDAO) : this(
                user.id,
                user.userName,
                user.name,
                user.password,
                user.email,
                user.birthDate,
                user.institution.id
        )
}
