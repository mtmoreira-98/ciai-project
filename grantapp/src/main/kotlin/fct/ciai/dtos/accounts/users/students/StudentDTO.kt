package fct.ciai.dtos.accounts.users.students

import fct.ciai.daos.StudentDAO
import fct.ciai.dtos.accounts.users.UserDTO
import java.util.*

class StudentDTO(
        val id: Long,
        val userName: String,
        val name: String,
        val password: String,
        val email: String,
        val birthDate: Date,
        val institution: Long,
        val average: Double,
        val cv: Long
) {

    constructor(student: StudentDAO) : this(
            student.id,
            student.userName,
            student.name,
            student.password,
            student.email,
            student.birthDate,
            student.institution.id,
            student.average,
            student.cv.id
    )

    constructor() : this(0, "", "", "","", Date(), 0, 0.0, 0)

    // Only need for test purposes
    override fun equals(other: Any?): Boolean {
        return (other is StudentDTO)
                && this.id == other.id
                && this.userName == other.userName
                && this.name == other.name
                && this.email == other.email
                && this.birthDate == other.birthDate
                && this.institution == other.institution
                && this.average == other.average
                && this.cv == other.cv
    }
}
