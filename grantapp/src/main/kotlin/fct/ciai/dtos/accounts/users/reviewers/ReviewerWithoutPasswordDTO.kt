package fct.ciai.dtos.accounts.users.reviewers

import fct.ciai.daos.ReviewerDAO
import fct.ciai.dtos.accounts.users.UserWithoutPasswordDTO
import java.util.*

class ReviewerWithoutPasswordDTO(
        val id: Long,
        val userName: String,
        val name: String,
        val email: String,
        val birthDate: Date,
        val institution: Long)
{
    //Only for tests purposes
    constructor() : this(0,"","","", Date(),0)

    constructor(reviewer: ReviewerDAO):
            this(
                    reviewer.id,
                    reviewer.userName,
                    reviewer.name,
                    reviewer.email,
                    reviewer.birthDate,
                    reviewer.institution.id
            )

    //Only for tests purposes
    override fun equals(other: Any?) : Boolean  {
        return (other is ReviewerDTO)
                && id == other.id
                && userName == other.userName
                && name == other.name
                && email == other.email
                && institution == other.institution
    }

}
