package fct.ciai.dtos

import fct.ciai.daos.CVDAO

class CVDTO constructor(
        val id : Long,
        val cvItemsIds: List<CVItemDTO>
){
    constructor(cv: CVDAO): this(cv.id, cv.cvItems.map { CVItemDTO(it.id, it.type, it.header, it.subTitle, it.value) })
}
