package fct.ciai.dtos

import fct.ciai.daos.AccountDAO
import fct.ciai.dtos.accounts.AccountWithoutPasswordDTO
import fct.ciai.dtos.dataitems.DataItemDTO
import java.util.*

class LoginResponseDTO (

        val accountDTO: AccountWithoutPasswordDTO,
        val accountRoles: List<String>

){
    constructor() : this(
            AccountWithoutPasswordDTO(AccountDAO(0, "", "", "", "", mutableListOf())),
            listOf())

    constructor(accountDAO: AccountDAO): this(
            AccountWithoutPasswordDTO(accountDAO),
            accountDAO.authorities.map { it.name })
}
