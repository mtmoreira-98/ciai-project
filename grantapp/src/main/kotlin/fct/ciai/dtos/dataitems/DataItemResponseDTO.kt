package fct.ciai.dtos.dataitems

import fct.ciai.daos.DataItemResponseDAO


class DataItemResponseDTO(
        val id: Long,
        val dataItemId: Long,
        val grantApplicationId: Long,
        val value: String
) {
    constructor(dataItemResponseDAO: DataItemResponseDAO) : this(
            dataItemResponseDAO.id,
            dataItemResponseDAO.dataItem.id,
            dataItemResponseDAO.grantApplication.id,
            dataItemResponseDAO.value
    )
}
