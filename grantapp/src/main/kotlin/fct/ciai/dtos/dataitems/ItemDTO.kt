package fct.ciai.dtos.dataitems

open class ItemDTO (
        val id: Long,
        val type: String,
        val header: String,
        val subTitle: String)
