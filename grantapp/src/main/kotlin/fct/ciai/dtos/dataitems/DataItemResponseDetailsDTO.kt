package fct.ciai.dtos.dataitems

import fct.ciai.daos.DataItemResponseDAO

class DataItemResponseDetailsDTO(
        val id: Long,
        val dataItemId: Long,
        val grantApplicationId: Long,
        val value: String,
        val title: String
) {

    constructor(dataItemResponseDAO: DataItemResponseDAO, title: String) : this(
            dataItemResponseDAO.id,
            dataItemResponseDAO.dataItem.id,
            dataItemResponseDAO.grantApplication.id,
            dataItemResponseDAO.value,
            title
    )

}
