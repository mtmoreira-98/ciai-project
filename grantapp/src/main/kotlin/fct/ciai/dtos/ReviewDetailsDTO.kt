package fct.ciai.dtos

import fct.ciai.daos.ReviewDAO
import fct.ciai.dtos.accounts.users.reviewers.ReviewerWithoutPasswordDTO
import fct.ciai.enums.ReviewStatus

class ReviewDetailsDTO(
        val id: Long,
        val note: Int,
        val description: String,
        val reviewerID: Long,
        val grantApplicationId: Long,
        val reviewer: ReviewerWithoutPasswordDTO,
        val finalReview: ReviewStatus
) {
    constructor(review: ReviewDAO) : this(
            review.id,
            review.note,
            review.description,
            review.reviewer.id,
            review.grantApplication.id,
            ReviewerWithoutPasswordDTO(review.reviewer),
            review.status
    )

    constructor() : this(0, 0, "", 0, 0, ReviewerWithoutPasswordDTO(), ReviewStatus.NORMAL)

    override fun equals(other: Any?) = (other is ReviewDetailsDTO)
            && id == other.id
            && note == other.note
            && description == other.description
            && reviewerID == other.reviewerID
            && grantApplicationId == other.grantApplicationId
}
