package fct.ciai.dtos

import fct.ciai.dtos.dataitems.ItemDTO

class CVItemDTO constructor(
        id: Long,
        type: String,
        header: String,
        subTitle: String,
        val value: String
) : ItemDTO(id, type, header, subTitle)
