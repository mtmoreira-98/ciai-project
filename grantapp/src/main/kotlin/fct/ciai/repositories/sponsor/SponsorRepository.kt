package fct.ciai.repositories.sponsor

import fct.ciai.daos.SponsorDAO
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

interface SponsorRepository : PagingAndSortingRepository<SponsorDAO, Long> {
    fun findByUserName(userName: String) : Optional<SponsorDAO>

}
