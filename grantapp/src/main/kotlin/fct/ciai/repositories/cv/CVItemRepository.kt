package fct.ciai.repositories.cv

import fct.ciai.daos.CVItemDAO
import org.springframework.data.repository.CrudRepository

interface CVItemRepository : CrudRepository<CVItemDAO, Long>{
}


