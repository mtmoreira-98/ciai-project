package fct.ciai.repositories.cv

import fct.ciai.daos.CVDAO
import org.springframework.data.repository.CrudRepository

interface CVRepository : CrudRepository<CVDAO, Long>{
}


