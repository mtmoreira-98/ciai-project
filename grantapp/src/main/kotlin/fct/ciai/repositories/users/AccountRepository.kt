package fct.ciai.repositories.users

import fct.ciai.daos.AccountDAO
import org.springframework.data.repository.CrudRepository
import java.util.*

interface AccountRepository : CrudRepository<AccountDAO, Long> {
    fun existsByEmailEquals(email: String) : Boolean

    fun existsByUserName(email: String) : Boolean

    fun findByUserName(userName: String) : Optional<AccountDAO>

    fun existsByUserNameEqualsOrEmailEquals(userName: String, email: String) : Boolean

}
