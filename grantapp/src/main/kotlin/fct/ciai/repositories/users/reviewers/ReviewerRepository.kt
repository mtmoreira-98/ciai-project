package fct.ciai.repositories.users.reviewers

import fct.ciai.daos.GrantCallDAO
import fct.ciai.daos.ReviewerDAO
import fct.ciai.daos.StudentDAO
import fct.ciai.dtos.GrantCallDTO
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

interface ReviewerRepository : PagingAndSortingRepository<ReviewerDAO, Long> {
    fun findByUserName(userName: String): Optional<ReviewerDAO>

    @Query(value = "SELECT DISTINCT gc  FROM GrantCallDAO gc INNER JOIN gc.grantApplications ga INNER JOIN gc.evaluationPanel ev INNER JOIN ev.panelReviewers pm WHERE ga.status='IN_VOTING' and pm.id=?1")
    fun findGrantCallsToEvaluate(id: Long): List<GrantCallDAO>

    @Query(value = "SELECT DISTINCT gc  FROM GrantCallDAO gc INNER JOIN gc.grantApplications ga INNER JOIN gc.evaluationPanel ev INNER JOIN ev.panelChair pc WHERE ga.status='IN_VOTING' and pc.id = ?1")
    fun findChairedGrantCalls(id: Long): List<GrantCallDAO>

    @Query(value = "SELECT ga  FROM GrantCallDAO ga INNER JOIN ga.evaluationPanel ev INNER JOIN ev.panelChair pc WHERE ga.id=?1 and pc.id=?2")
    fun findReviewerChairGrantCalls(grantCalID: Long, reviewerID: Long): Optional<GrantCallDAO>
}
