package fct.ciai.repositories.grants

import fct.ciai.daos.EvaluationPanelDAO
import fct.ciai.daos.GrantApplicationDAO
import fct.ciai.daos.GrantCallDAO
import fct.ciai.enums.GrantCallStatus
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

interface GrantCallRepository : PagingAndSortingRepository<GrantCallDAO, Long> {

    fun findByEvaluationPanelEquals(evaluationPanel: EvaluationPanelDAO) : Optional<GrantCallDAO>

    fun findByStatusEquals(status: GrantCallStatus) : MutableList<GrantCallDAO>

}
