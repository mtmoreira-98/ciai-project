package fct.ciai.repositories.grants

import fct.ciai.daos.GrantApplicationDAO
import fct.ciai.daos.GrantCallDAO
import fct.ciai.daos.StudentDAO
import fct.ciai.enums.GrantApplicationStatus
import fct.ciai.enums.GrantCallStatus
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository

interface GrantApplicationRepository : PagingAndSortingRepository<GrantApplicationDAO, Long>{
    fun existsByStudentEqualsAndGrantCallEquals(student : StudentDAO, grantCall : GrantCallDAO) : Boolean

    @Query(value = "SELECT gc.grantApplications FROM GrantCallDAO gc WHERE gc.evaluationPanel.id = ?1")
    fun findGrantApplicationsByEvaluationPanelId(pageable: Pageable, id: Long) : MutableList<GrantApplicationDAO>

    @Query(value = "SELECT ga FROM GrantCallDAO gc INNER JOIN gc.grantApplications ga WHERE gc.id = ?1 and ga.status = ?2")
    fun findAcceptedApplication(id: Long, status: GrantApplicationStatus): List<GrantApplicationDAO>

    @Query(value = "SELECT ga, gc.title  FROM StudentDAO st INNER JOIN st.grantApplications ga INNER JOIN ga.grantCall gc WHERE st.id = ?1 and st.id = ga.student.id and gc.id = ga.grantCall.id")
    fun findStudentApplications(id: Long): List<GrantApplicationDAO>

    @Query(value = "SELECT ga, gc.title  FROM StudentDAO st INNER JOIN st.grantApplications ga INNER JOIN ga.grantCall gc WHERE st.id = ?1 and st.id = ga.student.id and gc.id = ga.grantCall.id and ga.status = ?2")
    fun findStudentApplicationsByStatus(id: Long, status: GrantApplicationStatus): List<GrantApplicationDAO>

}


