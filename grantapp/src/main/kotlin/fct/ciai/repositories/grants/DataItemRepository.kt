package fct.ciai.repositories.grants

import fct.ciai.daos.DataItemDAO
import org.springframework.data.repository.CrudRepository

interface DataItemRepository : CrudRepository<DataItemDAO, Long> {
}