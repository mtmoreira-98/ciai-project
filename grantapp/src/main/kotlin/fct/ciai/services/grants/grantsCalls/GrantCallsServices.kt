package fct.ciai.services.grants.grantsCalls

import fct.ciai.daos.*
import fct.ciai.dtos.grantApplications.GrantApplicationDetailsDTO
import fct.ciai.enums.GrantApplicationStatus
import fct.ciai.enums.GrantCallStatus
import fct.ciai.repositories.evaluationPanels.EvaluationPanelRepository
import fct.ciai.repositories.grants.DataItemRepository
import fct.ciai.repositories.grants.GrantApplicationRepository
import fct.ciai.repositories.grants.GrantCallRepository
import fct.ciai.repositories.users.reviewers.ReviewerRepository
import fct.ciai.services.BadRequestException
import fct.ciai.services.NotClosedGrantCallException
import fct.ciai.services.NotFoundException
import fct.ciai.services.grants.grantsApplications.GrantApplicationsServices
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class GrantCallsServices(
        @Autowired val grantCallRepository: GrantCallRepository,
        @Autowired val dataItemsRepository: DataItemRepository,
        @Autowired val evaluationPanelRepository: EvaluationPanelRepository,
        @Autowired val reviewerRepository: ReviewerRepository,
        @Autowired val grantApplicationRepository: GrantApplicationRepository,
        @Autowired val grantApplicationsServices: GrantApplicationsServices
) {

    fun getAllGrantCalls(): Iterable<GrantCallDAO> {
        return grantCallRepository.findAll()
    }

    fun getGrantCallById(id: Long): GrantCallDAO {
        val grantCall = grantCallRepository.findById(id)

        if (grantCall.isEmpty)
            throw NotFoundException("GrantCall with id: $id does not exist.")

        return verifyAndUpdateStatus(grantCall.get())
    }

    @Transactional
    fun addGrantCall(grantCallDAO: GrantCallDAO, dataItemsDAO: MutableList<DataItemDAO>, evaluationPanelDAO: EvaluationPanelDAO) {
        /*Create Evaluation Panel*/
        evaluationPanelRepository.save(evaluationPanelDAO)

        /*Add evaluation panel to reviewers*/
        val panelReviewers = evaluationPanelDAO.panelReviewers
        for (reviewer in panelReviewers) {
            reviewer.evaluationPanels.add(evaluationPanelDAO)
            reviewerRepository.save(reviewer)
        }

        val grantCall = grantCallRepository.save(grantCallDAO)

        // Create and add DataItems to Grant Call
        val dataItemsDAOReply = dataItemsRepository.saveAll(dataItemsDAO)
        grantCall.dataItems = dataItemsDAOReply.toMutableList()
        grantCallRepository.save(grantCall)
    }

    fun addDataItems(granCallID: Long, dataItems: MutableList<DataItemDAO>) {
        val grantCallDAO = getGrantCallById(granCallID)
        grantCallDAO.dataItems = dataItems
        grantCallRepository.save(grantCallDAO)
    }

    @Transactional
    fun deleteGrantCall(id: Long) {
        getGrantCallById(id)
        grantCallRepository.deleteById(id)
    }

    fun getAllGrantApplications(id: Long): List<GrantApplicationDAO> {
        /*Verify if exists*/
        getGrantCallById(id)
        return getGrantCallById(id).grantApplications
    }

    fun verifyDataItemsResponses(grantAppDAO: GrantApplicationDAO, dataItemsResponsesIds: List<Long>, dataItemsValues: List<String>): MutableList<DataItemResponseDAO> {
        val dataItems = grantAppDAO.grantCall.dataItems
        val result = mutableListOf<DataItemResponseDAO>()
        for (dataItem in dataItems) {
            var match = false
            for (i in dataItemsResponsesIds.indices) {
                if (dataItemsResponsesIds[i] == dataItem.id) {
                    val dataItemDAO = DataItemResponseDAO(0, dataItemsValues[i], dataItem, grantAppDAO)
                    result.add(dataItemDAO)
                    match = true
                    break
                }
            }
            if (!match && dataItem.mandatory)
                throw BadRequestException("Mandatory Data Item with id ${dataItem.id} has no response associated.")
        }
        return result
    }

    fun getAllGrantOnStatus(status: String): Iterable<GrantCallDAO> {
        return when (status.toLowerCase()) {
            "open" -> grantCallRepository.findByStatusEquals(GrantCallStatus.OPEN)
            "all" -> getAllGrantCalls()
            "close" -> grantCallRepository.findByStatusEquals(GrantCallStatus.CLOSED)
            else -> throw BadRequestException("Status not supported")
        }
    }

    fun getAcceptedGrantCallApplications(grantCallID: Long): List<GrantApplicationDAO> {
        return grantApplicationRepository.findAcceptedApplication(grantCallID, GrantApplicationStatus.ACCEPTED)
    }

    fun verifyAndUpdateStatus(grantCall: GrantCallDAO): GrantCallDAO {
        if (Date().after(grantCall.expireDate)) {
            grantCall.status = GrantCallStatus.CLOSED
            grantCallRepository.save(grantCall)
            for (grantApp in grantCall.grantApplications) {
                grantApplicationsServices.verifyAndUpdateStatus(grantApp)
            }
        }
        return grantCall
    }

    fun getGrantCallApplicationsByStatus(grantCallID: Long, status: GrantApplicationStatus): List<GrantApplicationDAO> {
        return grantApplicationRepository.findAcceptedApplication(grantCallID, status)
    }
}
