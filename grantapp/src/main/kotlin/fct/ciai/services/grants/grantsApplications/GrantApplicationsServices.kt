package fct.ciai.services.grants.grantsApplications

import fct.ciai.daos.DataItemResponseDAO
import fct.ciai.daos.GrantApplicationDAO
import fct.ciai.daos.ReviewDAO
import fct.ciai.enums.GrantApplicationStatus
import fct.ciai.repositories.grants.DataItemResponseRepository
import fct.ciai.repositories.grants.GrantApplicationRepository
import fct.ciai.services.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class GrantApplicationsServices(
        @Autowired val grantApplicationRepository: GrantApplicationRepository,
        @Autowired val dataItemResponseRepository: DataItemResponseRepository
) {

    fun getAllGrantApplications(pageable: Pageable): Iterable<GrantApplicationDAO> = grantApplicationRepository.findAll(pageable)

    fun getAllGrantApplications(): Iterable<GrantApplicationDAO> = grantApplicationRepository.findAll()

    fun getGrantApplicationById(id: Long): GrantApplicationDAO {
        val grantApplication = grantApplicationRepository.findById(id).orElseThrow{
            NotFoundException("Grant application with id $id does not exit in the system")
        }
        return verifyAndUpdateStatus(grantApplication)
    }

    @Transactional
    fun addGrantApplication(dataItemResponsesDAO: MutableList<DataItemResponseDAO>, grantApplicationDAO: GrantApplicationDAO) {
        if(grantApplicationRepository.existsByStudentEqualsAndGrantCallEquals(grantApplicationDAO.student,grantApplicationDAO.grantCall))
            throw ConflictException("This student already made a application to this grant call")

        /*Create Grant Application*/
        val grantApplication = grantApplicationRepository.save(grantApplicationDAO)


        /*Associate Grant Application Id to DataItems*/
        val dataItemsResponses = dataItemResponsesDAO.map {  DataItemResponseDAO(it.id, it.value, it.dataItem, grantApplication) }.toMutableList()

        /*Save data item responses*/
        dataItemResponseRepository.saveAll(dataItemsResponses)

        /*Associate data Items to Grant Call*/
        grantApplicationDAO.fieldsResponses = dataItemsResponses
        grantApplicationRepository.save(grantApplicationDAO)
    }

    fun submitGrantApplication(dataItemsResponsesDAO: MutableList<DataItemResponseDAO>, grantApplicationDAO: GrantApplicationDAO) {
        val grantApplication = grantApplicationRepository.findById(grantApplicationDAO.id)

        /*If grant applications does not exists*/
        if(grantApplication.isEmpty)
            throw NotFoundException("Grant Application does not exists.")

        val grantApplicationGet = grantApplication.get()

        /*Update status to submited*/
        grantApplicationGet.status = GrantApplicationStatus.SUBMITTED
        grantApplicationRepository.save(grantApplicationGet)
    }

    fun submitGrantApplication(id: Long){
        val grantApplication = getGrantApplicationById(id)

        /*Update status to submited*/
        grantApplication.status = GrantApplicationStatus.SUBMITTED
        grantApplicationRepository.save(grantApplication)
    }

    @Transactional
    fun deleteGrantApplication(id: Long) {
        getGrantApplicationById(id)
        grantApplicationRepository.deleteById(id)
    }

    fun getAllGrantApplicationReviews(grantApplicationID: Long): List<ReviewDAO> {
        /* Verify if Grant Application does not exit */
        val grantApplication =  getGrantApplicationById(grantApplicationID)

        when(grantApplication.status) {
            GrantApplicationStatus.CREATED -> throw NotSubmitedException()
            GrantApplicationStatus.SUBMITTED -> throw WaitUntilInVotingTimeException()
            //GrantApplicationStatus.IN_VOTING -> throw InVotingException()
            else -> grantApplication.reviews
        }
        return grantApplication.reviews
    }

    fun verifyAndUpdateStatus(grantApplication : GrantApplicationDAO) : GrantApplicationDAO{
        if(grantApplication.status == GrantApplicationStatus.SUBMITTED
                && Date().after(grantApplication.grantCall.expireDate))
        {
            grantApplication.status = GrantApplicationStatus.IN_VOTING
            grantApplicationRepository.save(grantApplication)
        }
        return grantApplication
    }
}
