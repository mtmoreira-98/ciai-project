package fct.ciai.services.cv

import fct.ciai.daos.CVDAO
import fct.ciai.daos.CVItemDAO
import fct.ciai.repositories.cv.CVItemRepository
import fct.ciai.repositories.cv.CVRepository
import fct.ciai.services.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class CVService(
        @Autowired val cvRepository: CVRepository,
        @Autowired val cvItemsRepository: CVItemRepository
) {

    fun getCVById(cvID: Long): CVDAO {
        val cv = cvRepository.findById(cvID)

        if (cv.isEmpty) {
            throw NotFoundException("CV with id $cvID does not exit in the system.")
        }
        return cv.get()
    }

    @Transactional
    fun createCVItem(cvItemDAO: CVItemDAO) {
        cvItemsRepository.save(cvItemDAO)
        val cv = cvItemDAO.cv
        cv.cvItems.add(cvItemDAO)
        cvRepository.save(cv)
    }

    @Transactional
    fun updateCVItem(cvID: Long, cvItemDAO: CVItemDAO) {
        getCVById(cvID)
        val cvItemId = cvItemDAO.id

        val cvItemOption = getCVItemById(cvItemId)

        cvItemOption.update(cvItemDAO)
        cvItemsRepository.save(cvItemOption)
    }

    @Transactional
    fun deleteCVItem(cvID: Long, cvItemID: Long) {
        /*Remove CVItem from repository*/
        cvItemsRepository.deleteById(cvItemID)
    }

    fun getCVItemById(cvItemID: Long) : CVItemDAO{
        val cvItemOption = cvItemsRepository.findById(cvItemID)
        if (cvItemOption.isEmpty)
            throw NotFoundException("CV Item with id $cvItemID does not exit in the system.")
        return cvItemOption.get()
    }

}
