package fct.ciai.services.evaluationPanels

import fct.ciai.daos.EvaluationPanelDAO
import fct.ciai.daos.GrantApplicationDAO
import fct.ciai.daos.ReviewerDAO
import fct.ciai.repositories.evaluationPanels.EvaluationPanelRepository
import fct.ciai.repositories.grants.GrantApplicationRepository
import fct.ciai.services.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class EvaluationPanelService(
        @Autowired val evaluationPanelRepository: EvaluationPanelRepository,
        @Autowired val grantApplicationRepository: GrantApplicationRepository
) {
    fun getEvaluationPanelById(id: Long): EvaluationPanelDAO {
        val evaluationPanel = evaluationPanelRepository.findById(id)
        if (!evaluationPanel.isPresent)
            throw NotFoundException("Evaluation Panel with id: $id does not exist.")
        return evaluationPanel.get()
    }

    fun getAllReviewersByEvaluationPanelId(id: Long): List<ReviewerDAO> {
        return getEvaluationPanelById(id).panelReviewers
    }

    fun getAllGrantApplicationsByEvaluationPanelId(pageable: Pageable, id: Long): List<GrantApplicationDAO> {
        getEvaluationPanelById(id) //Check if exists
        return grantApplicationRepository.findGrantApplicationsByEvaluationPanelId(pageable, id)
    }
}
