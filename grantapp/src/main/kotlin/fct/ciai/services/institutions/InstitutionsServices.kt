package fct.ciai.services.institutions

import fct.ciai.daos.InstitutionDAO
import fct.ciai.repositories.institutions.InstitutionRepository
import fct.ciai.services.ConflictException
import fct.ciai.services.NotFoundException

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class InstitutionsServices(
        @Autowired val institutionRepository: InstitutionRepository
) {

    fun getAllInstitutions(pageable: Pageable): Iterable<InstitutionDAO> = institutionRepository.findAll(pageable)

    fun getAllInstitutions(): Iterable<InstitutionDAO> = institutionRepository.findAll()

    fun getInstitutionById(id: Long): InstitutionDAO {
        return institutionRepository.findById(id).orElseThrow {
            NotFoundException("Institution with id $id not found")
        }
    }

    @Transactional
    fun addInstitution(institution: InstitutionDAO) {
        if (institutionRepository.existsByNameEqualsOrEmailEquals(institution.name,institution.email))
            throw ConflictException("Institution with name ${institution.name} or email ${institution.email} already exists")
        else
            institutionRepository.save(institution)
    }

    @Transactional
    fun deleteInstitution(id: Long) {
        val findResult = getInstitutionById(id)
        institutionRepository.delete(findResult)
    }
}
