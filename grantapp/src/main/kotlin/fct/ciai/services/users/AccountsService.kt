package fct.ciai.services.users

import fct.ciai.daos.AccountDAO
import fct.ciai.enums.RolesEnum
import fct.ciai.repositories.users.AccountRepository
import fct.ciai.services.ConflictException
import fct.ciai.services.ForbiddenException
import fct.ciai.services.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class AccountsService (
        @Autowired val accountsRepository : AccountRepository
){
    fun findUser(userName: String): AccountDAO {
        return accountsRepository.findByUserName(userName).orElseThrow(){
            throw NotFoundException("User with the username $userName does not exit in the system.")
        }
    }

    /*Verify if accounts exists*/
    fun accountExists(accountDAO: AccountDAO) {
        if (accountsRepository.existsByEmailEquals(accountDAO.email)
                || accountsRepository.existsByUserName(accountDAO.userName)) {
            throw ConflictException("Account with email ${accountDAO.email} or userName ${accountDAO.userName} already exists in the system.")
        }
    }

    fun login(username: String, password: String) : AccountDAO{
        val account = accountsRepository.findByUserName(username).orElseThrow(){
            throw ForbiddenException("Login fails.")
        }

        if(!BCryptPasswordEncoder().matches(password, account.password))
            throw ForbiddenException("Login fails.")

        return account
    }
}
