package fct.ciai.services.users.students

import fct.ciai.daos.GrantApplicationDAO
import fct.ciai.daos.GrantCallDAO
import fct.ciai.daos.StudentDAO
import fct.ciai.enums.GrantApplicationStatus
import fct.ciai.repositories.cv.CVRepository
import fct.ciai.repositories.grants.GrantApplicationRepository
import fct.ciai.repositories.users.AccountRepository
import fct.ciai.repositories.users.students.StudentRepository
import fct.ciai.services.ConflictException
import fct.ciai.services.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class StudentsServices(
        @Autowired val accountRepository: AccountRepository,
        @Autowired val studentRepository: StudentRepository,
        @Autowired val cvRepository: CVRepository,
        @Autowired val grantApplicationRepository: GrantApplicationRepository
) {

    fun getAllStudents(pageable: Pageable): Iterable<StudentDAO> = studentRepository.findAll(pageable)

    fun getAllStudents(): Iterable<StudentDAO> = studentRepository.findAll()

    fun getStudentById(id: Long): StudentDAO =
            studentRepository.findById(id).orElseThrow {
                NotFoundException("Student with id $id not found.")
            }

    fun getStudentByUserName(userID: Long): StudentDAO =
            studentRepository.findById(userID).orElseThrow {
                NotFoundException("Student with id $userID not found.")
            }

    @Transactional
    fun addStudent(studentDAO: StudentDAO) : StudentDAO{
        if(accountRepository.existsByUserNameEqualsOrEmailEquals(studentDAO.userName, studentDAO.email)){
            throw ConflictException("Student with username ${studentDAO.userName} or " +
                    " email ${studentDAO.email} already exists in the system.")
        }

        /*Create Student CV*/
        cvRepository.save(studentDAO.cv)

        /*Create and return Student*/
        return  studentRepository.save(studentDAO)

    }

    @Transactional
    fun deleteStudent(userID: Long){
        val student = getStudentById(userID)
        studentRepository.delete(student)

        grantApplicationRepository.deleteAll(student.grantApplications)
    }

    fun getStudentApplications(userID: Long): List<GrantApplicationDAO> {
        val studentOptional = studentRepository.findById(userID)
        if (studentOptional.isEmpty)
            throw NotFoundException("Student with id $userID does not exit in the system.")
        return studentOptional.get().grantApplications
    }

    fun getStudentApplicationsWithGrantCallName(userID: Long, status: GrantApplicationStatus?): List<GrantApplicationDAO> {
        getStudentById(userID)
        if(status == null)
            return grantApplicationRepository.findStudentApplications(userID)
        return grantApplicationRepository.findStudentApplicationsByStatus(userID, status)
    }

    fun getOpenAndNotCreatedGrantCalls(studentID: Long) : List<GrantCallDAO>{
        return studentRepository.getOpenAndNotCreatedGrantCalls(studentID)
    }
}
