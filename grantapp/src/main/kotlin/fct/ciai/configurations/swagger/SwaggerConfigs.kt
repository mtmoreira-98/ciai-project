@file:Suppress("DEPRECATION")

package fct.ciai.configurations.swagger

import com.google.common.base.Predicates
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2


@Configuration
@EnableSwagger2
class Swagger2UiConfiguration : WebMvcConfigurerAdapter() {

    //http://localhost:8080/swagger-ui.html#/ -> API Web-Page
    //http://localhost:8080/h2/ -> H2 Database Interface
    fun apiInfo(): ApiInfo = ApiInfoBuilder()
            .title("Grant Application API")
            .version("1.0.0")
            .description("Grant Application management system.")
            .contact(Contact("Pedro Silva, Miguel Moreira, Rodrigo Lopes" , "", ""))
            .license("Apache 2.0")
            .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
            .build()

    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .select()
                .apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
                .build()
    }

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        //enabling swagger-ui part for visual documentation
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/")
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/")
    }
}