package fct.ciai.configurations.security

import fct.ciai.services.NotFoundException
import fct.ciai.services.users.AccountsService
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service("customAccountDetailsService")
@Transactional
class CustomAccountDetailsService(
        val accounts: AccountsService
) : UserDetailsService{

    override fun loadUserByUsername(username: String?): UserDetails {
        username?.let{
            val accountDAO = accounts.findUser(it)
            return User(accountDAO.userName, accountDAO.password, accountDAO.authorities.map{ authority -> SimpleGrantedAuthority(authority.name) }.toMutableList())
        }
        throw NotFoundException("Account with username $username does not exit in the system.")
    }
}
