package fct.ciai.configurations.security.filters

import com.fasterxml.jackson.databind.ObjectMapper
import fct.ciai.configurations.security.JWTUtil
import fct.ciai.daos.AccountDAO
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class UserPasswordAuthenticationFilterToJWT(
        defaultFilterProcessesUrl: String?,
        private val anAuthenticationManager: AuthenticationManager
): AbstractAuthenticationProcessingFilter(defaultFilterProcessesUrl){

    override fun attemptAuthentication(request: HttpServletRequest?,
                                       response: HttpServletResponse?): Authentication? {

        //get user from request
        val user = ObjectMapper().readValue(request!!.inputStream, AccountDAO::class.java)

        val auth = anAuthenticationManager.authenticate(UsernamePasswordAuthenticationToken(user.userName, user.password))

        return if (auth.isAuthenticated){
            SecurityContextHolder.getContext().authentication = auth
            auth
        }else
            null
    }

    override fun successfulAuthentication(request: HttpServletRequest,
                                          response: HttpServletResponse,
                                          chain: FilterChain,
                                          auth: Authentication) {
        //When returning from the filter loop, add the token to the response
        JWTUtil.addResponseToken(auth, response)
    }
}
