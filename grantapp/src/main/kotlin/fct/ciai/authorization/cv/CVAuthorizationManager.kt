package fct.ciai.authorization.cv

import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.RequestBody
import java.lang.annotation.Inherited

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetCVById.Condition)
annotation class AllowForGetCVById {
    companion object {
        const val Condition = "isAuthenticated() and (hasAnyRole({'ADMIN', 'REVIEWER'}) or " +
                "@cvSecurityService.isCVOwner(principal, #cvID))"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForCreateCVItem.Condition)
annotation class AllowForCreateCVItem {
    companion object {
        const val Condition = "isAuthenticated() and (hasRole({'ADMIN'}) or " +
                "@cvSecurityService.isCVOwner(principal, #cvID))"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForCreateCVItem.Condition)
annotation class AllowForUpdateCVItem {
    companion object {
        const val Condition = "isAuthenticated() and (hasRole({'ADMIN'}) or " +
                "@cvSecurityService.isCVOwner(principal, #cvID))"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForDeleteCVItem.Condition)
annotation class AllowForDeleteCVItem {
    companion object {
        const val Condition = "isAuthenticated() and (hasRole({'ADMIN'}) or " +
                "@cvSecurityService.isCVOwner(principal, #cvID))"
    }
}
