package fct.ciai.authorization.user.students

import fct.ciai.repositories.users.students.StudentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Component

@Component("studentsSecurityService")
class StudentsSecurityService(
        @Autowired val studentRepository: StudentRepository
) {
    fun canGetStudentById(principal: User, userID: Long) : Boolean {
        return userNameMatches(principal, userID)
    }

    fun canGetStudentGrantApplications(principal: User, userID: Long) : Boolean {
        return userNameMatches(principal, userID)
    }

    fun userNameMatches(principal: User, userID: Long) : Boolean{
        val student = studentRepository.findById(userID)
        return if(student.isEmpty){
            false
        } else{
            student.get().userName == principal.username
        }
    }


}
