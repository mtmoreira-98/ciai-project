package fct.ciai.authorization.grants.grantCalls

import org.springframework.security.access.prepost.PreAuthorize
import java.lang.annotation.Inherited


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetAllGrantCalls.Condition)
annotation class AllowForGetAllGrantCalls {
    companion object {
        const val Condition = "hasAnyRole({'ADMIN', 'STUDENT', 'REVIEWER'})"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetGrantCallById.Condition)
annotation class AllowForGetGrantCallById {
    companion object {
        const val Condition = "isAuthenticated() and ( hasAnyRole({'ADMIN', 'STUDENT', 'REVIEWER'}) or " +
         "@grantCallSecurityService.isGrantCallOwner(principal, #id))"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForCreateGrantCall.Condition)
annotation class AllowForCreateGrantCall {
    companion object {
        const val Condition = "hasAnyRole({'ADMIN', 'SPONSOR'})"
    }
}


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForDeleteGrantCall.Condition)
annotation class AllowForDeleteGrantCall {
    companion object {
        const val Condition = "hasRole({'ADMIN'}) or " +
                "@grantCallSecurityService.isGrantCallOwner(principal, #id)"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetGrantCallApplications.Condition)
annotation class AllowForGetGrantCallApplications {
    companion object {
        const val Condition = "hasAnyRole({'ADMIN', 'REVIEWER'}) or " +
                "@grantCallSecurityService.isGrantCallOwner(principal, #id)"
    }
}
