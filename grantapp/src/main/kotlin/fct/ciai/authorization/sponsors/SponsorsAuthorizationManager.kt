package fct.ciai.authorization.sponsors

import org.springframework.security.access.prepost.PreAuthorize
import java.lang.annotation.Inherited

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetAllSponsors.Condition)
annotation class AllowForGetAllSponsors {
    companion object {
        const val Condition = "hasRole({'ADMIN'})"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetSponsor.Condition)
annotation class AllowForGetSponsor {
    companion object {
        const val Condition = "isAuthenticated() and ("+
                "hasAnyRole({'ADMIN', 'STUDENT', 'REVIEWER'}) or " +
                "@sponsorSecurityService.canGetSponsorById(principal, #id)"+
                ")"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForCreateSponsor.Condition)
annotation class AllowForCreateSponsor {
    companion object {
        const val Condition = "hasRole({'ADMIN'})"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForDeleteSponsor.Condition)
annotation class AllowForDeleteSponsor {
    companion object {
        const val Condition = "hasRole({'ADMIN'})"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetSponsorGrantCalls.Condition)
annotation class AllowForGetSponsorGrantCalls {
    companion object {
        const val Condition = "isAuthenticated() and ("+
                "hasRole({'ROLE_ADMIN'}) or " +
                "@sponsorSecurityService.canGetSponsorGrantCalls(principal,#id)"+
                ")"
    }
}