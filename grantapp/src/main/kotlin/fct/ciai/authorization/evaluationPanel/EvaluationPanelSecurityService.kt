package fct.ciai.authorization.evaluationPanel

import fct.ciai.repositories.evaluationPanels.EvaluationPanelRepository
import fct.ciai.repositories.grants.GrantCallRepository
import fct.ciai.repositories.sponsor.SponsorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Component

@Component("evaluationPanelSecurityService")
class EvaluationPanelSecurityService(
        @Autowired val evaluationPanelRepository: EvaluationPanelRepository,
        @Autowired val grantCallRepository: GrantCallRepository,
        @Autowired val sponsorRepository: SponsorRepository


){

    fun isReviewerInEvaluationPanel(principal: User, id: Long): Boolean {

        var isIn = false
        val evaPanel = evaluationPanelRepository.findById(id)
        if (evaPanel.isEmpty()) {
            return isIn
        }
        val evaluationPanel = evaPanel.get()

        isIn = evaluationPanel.panelReviewers.map {it.userName}.contains(principal.username)

        if (evaluationPanel.panelChair.userName == principal.username )
            isIn = true

        return isIn
    }

    fun isGrantCallOwner(principal: User, id: Long): Boolean {
        val sponsor = sponsorRepository.findByUserName(principal.username)
        val evaPanel = evaluationPanelRepository.findById(id)
        if(evaPanel.isEmpty) return false
        val grantCalls = grantCallRepository.findByEvaluationPanelEquals(evaPanel.get())
        return if (sponsor.isEmpty || grantCalls.isEmpty ) {
            false
        } else {
            grantCalls.get().sponsor.id == sponsor.get().id
        }
    }

}