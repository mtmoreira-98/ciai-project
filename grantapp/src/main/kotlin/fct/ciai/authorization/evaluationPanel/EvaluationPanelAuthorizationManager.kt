package fct.ciai.authorization.evaluationPanel

import org.springframework.security.access.prepost.PreAuthorize
import java.lang.annotation.Inherited

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetEvaluationPanelById.Condition)
annotation class AllowForGetEvaluationPanelById {
    companion object {
        const val Condition = "isAuthenticated() and ("+
                "hasAnyRole({'ADMIN', 'SPONSOR'}) or "+
                "@evaluationPanelSecurityService.isReviewerInEvaluationPanel(principal,#id) or "+
                "@evaluationPanelSecurityService.isGrantCallOwner(principal,#id))"
    }
}


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetAllReviewersByEvaluationPanelId.Condition)
annotation class AllowForGetAllReviewersByEvaluationPanelId {
    companion object {
        const val Condition = "isAuthenticated() and ("+
                "hasAnyRole({'ADMIN', 'SPONSOR'}) or "+
                "@evaluationPanelSecurityService.isReviewerInEvaluationPanel(principal,#id) or "+
                "@evaluationPanelSecurityService.isGrantCallOwner(principal,#id))"
    }
}


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetAllGrantApplicationsByEvaluationPanelId.Condition)
annotation class AllowForGetAllGrantApplicationsByEvaluationPanelId {
    companion object {
        const val Condition = "isAuthenticated() and ("+
                "hasRole({'ADMIN'}) or "+
                "@evaluationPanelSecurityService.isReviewerInEvaluationPanel(principal,#id) or "+
                "@evaluationPanelSecurityService.isGrantCallOwner(principal,#id))"
    }
}
