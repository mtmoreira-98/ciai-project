package fct.ciai.authorization.institutions

import org.springframework.security.access.prepost.PreAuthorize
import java.lang.annotation.Inherited


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetAllInstitutions.Condition)
annotation class AllowForGetAllInstitutions {
    companion object {
        const val Condition = "hasAnyRole({'ADMIN', 'SPONSOR', 'STUDENT', 'REVIEWER'})"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetInstitution.Condition)
annotation class AllowForGetInstitution {
    companion object {
        const val Condition = "hasAnyRole({'ADMIN', 'SPONSOR', 'STUDENT', 'REVIEWER'})"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForCreateInstitution.Condition)
annotation class AllowForCreateInstitution {
    companion object {
        const val Condition = "hasRole({'ADMIN'})"
    }
}


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForDeleteInstitution.Condition)
annotation class AllowForDeleteInstitution {
    companion object {
        const val Condition = "hasRole({'ADMIN'})"
    }
}