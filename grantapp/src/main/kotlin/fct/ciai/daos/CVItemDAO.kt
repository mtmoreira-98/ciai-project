package fct.ciai.daos

import com.sun.istack.NotNull
import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
class CVItemDAO constructor (
        id: Long,
        type: String,
        header: String,
        subTitle: String,

        @NotNull
        var value: String,

        @ManyToOne
        @JoinColumn(name = "cv_id")
        val cv : CVDAO
): ItemDAO(id, type, header, subTitle){

        fun update(cv: CVItemDAO) {
                this.type = cv.type
                this.header = cv.header
                this.subTitle = cv.subTitle
                this.value = cv.value
        }


}
