@file:Suppress("JpaDataSourceORMInspection")

package fct.ciai.daos

import com.sun.istack.NotNull
import fct.ciai.enums.GrantCallStatus
import javax.persistence.GenerationType.AUTO
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "grant_call")
class GrantCallDAO(
        @Id
        @GeneratedValue(strategy = AUTO)
        val id: Long,

        @NotNull
        @Enumerated(EnumType.STRING)
        var status: GrantCallStatus,

        @NotNull
        val title: String,

        @NotNull
        val description: String,

        @NotNull
        val startDate: Date,

        @NotNull
        val expireDate: Date,

        @NotNull
        val funding: Long,

        @NotNull
        @OneToOne(cascade = [CascadeType.REMOVE])
        val evaluationPanel: EvaluationPanelDAO,

        @NotNull
        @OneToMany(fetch = FetchType.EAGER, mappedBy = "grantCall", cascade = [CascadeType.REMOVE])
        var dataItems: MutableList<DataItemDAO>,

        @NotNull
        @ManyToOne
        @JoinColumn(name = "sponsor_id")
        val sponsor: SponsorDAO,

        @OneToMany(mappedBy = "grantCall", cascade = [CascadeType.ALL])
        var grantApplications: MutableList<GrantApplicationDAO>) {

    constructor(
            id: Long,
            title: String,
            description: String,
            startDate: Date,
            expireDate: Date,
            funding: Long,
            evaluationPanel: EvaluationPanelDAO,
            dataItems: MutableList<DataItemDAO>,
            sponsor: SponsorDAO) :
            this(
                    id,
                    GrantCallStatus.OPEN,
                    title,
                    description,
                    startDate,
                    expireDate,
                    funding,
                    evaluationPanel,
                    dataItems,
                    sponsor,
                    mutableListOf()
            )

}
