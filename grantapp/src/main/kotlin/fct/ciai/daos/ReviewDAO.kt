package fct.ciai.daos

import com.sun.istack.NotNull
import fct.ciai.enums.ReviewStatus
import javax.persistence.*

@Entity
data class ReviewDAO(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long,

        @NotNull
        var note: Int,

        @NotNull
        var description: String,

        @ManyToOne
        @JoinColumn(name = "reviewer_id")
        val reviewer: ReviewerDAO,

        @ManyToOne
        @JoinColumn(name = "grant_application_id")
        val grantApplication: GrantApplicationDAO,

        @NotNull
        var status: ReviewStatus
) {

        fun update(review: ReviewDAO) {
                this.note= review.note
                this.description = review.description
        }

}
