@file:Suppress("EqualsOrHashCode", "JpaDataSourceORMInspection")

package fct.ciai.daos

import com.sun.istack.NotNull
import javax.persistence.*

@Entity
@Table(name = "institution")
data class InstitutionDAO(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long,

        @NotNull
        @Column(unique = true)
        val name: String,

        @NotNull
        val address: String,

        @NotNull
        val contact: String,

        @NotNull
        val email: String,

        @OneToMany(fetch = FetchType.LAZY, mappedBy="institution", cascade = [CascadeType.ALL])
        val users : MutableList<UserDAO>
) {

    // Only need for test purposes
    override fun equals(other: Any?)
            = (other is InstitutionDAO)
            && id == other.id
            && name == other.name
            && address == other.address
            && contact == other.contact
            && email == other.email
}

