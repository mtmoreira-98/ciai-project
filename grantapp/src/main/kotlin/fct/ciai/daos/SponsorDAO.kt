@file:Suppress("JpaDataSourceORMInspection", "EqualsOrHashCode")

package fct.ciai.daos

import fct.ciai.enums.RolesEnum
import lombok.Data
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import javax.persistence.*

@Entity
@Data
@Table(name = "sponsor")
class SponsorDAO(
        id: Long,

        userName: String,

        name: String,

        password: String,

        email: String,

        var phoneNumber: String,

        @OneToMany(mappedBy="sponsor", cascade = [CascadeType.ALL])
        var grantCalls: MutableList<GrantCallDAO>,

        authorities: MutableList<RolesEnum>

) : AccountDAO(id, userName, name, password, email, authorities) {

    constructor(id: Long, userName: String, name: String, password: String, email: String, phoneNumber: String) :
            this(
                    id,
                    userName,
                    name,
                    BCryptPasswordEncoder().encode(password),
                    email,
                    phoneNumber,
                    mutableListOf(),
                    mutableListOf(RolesEnum.ROLE_SPONSOR)
            )

    // Only need for test purposes
    override fun equals(other: Any?): Boolean {
        return (other is SponsorDAO)
                && this.id == other.id
                && this.userName == other.userName
                && this.name == other.name
                && this.email == other.email
                && this.phoneNumber == other.phoneNumber
                && this.grantCalls.map { it.id } == other.grantCalls.map { it.id }
    }


    override fun toString(): String {
        return "{id: ${this.id}, " +
                "name: ${this.name}, " +
                "password: ${this.password}, " +
                "email: ${this.email}, " +
                "phoneNumber: ${this.phoneNumber}, " +
                "grantCalls: ${this.grantCalls}}"
    }
}
