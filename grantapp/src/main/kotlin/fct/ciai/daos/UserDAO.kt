package fct.ciai.daos

import com.sun.istack.NotNull
import fct.ciai.enums.RolesEnum
import java.util.*
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
open class UserDAO(
        id: Long,
        userName: String,
        name: String,
        password: String,
        email: String,

        @NotNull
        open val birthDate: Date,

        @ManyToOne
        @JoinColumn(name = "institution_id")
        open val institution: InstitutionDAO,

        authorities: MutableList<RolesEnum>

) : AccountDAO(id, userName, name, password, email, authorities)
