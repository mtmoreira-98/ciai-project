package fct.ciai.enums

enum class RolesEnum {
    ROLE_STUDENT, ROLE_SPONSOR, ROLE_REVIEWER, ROLE_ADMIN
}
