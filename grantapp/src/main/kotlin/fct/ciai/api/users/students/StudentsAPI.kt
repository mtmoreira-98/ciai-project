package fct.ciai.api.users.students

import fct.ciai.authorization.grants.grantCalls.AllowForGetGrantCallApplications
import fct.ciai.authorization.user.students.*
import fct.ciai.daos.GrantCallDAO
import fct.ciai.dtos.GrantCallDTO
import fct.ciai.dtos.grantApplications.GrantApplicationDetailsDTO
import fct.ciai.dtos.accounts.users.students.StudentDTO
import fct.ciai.dtos.accounts.users.students.StudentWithoutPasswordDTO
import fct.ciai.dtos.grantApplications.GrantApplicationForListingDTO
import fct.ciai.enums.GrantApplicationStatus
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(value = "Grant Application Management System",
        description = "Manage Operations of Students in the Grant Application System")
@RequestMapping("/students")
interface StudentsAPI {

    @ApiOperation(value = "View a list of registered students.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all the registered students."),
        ApiResponse(code = 401, message = "Unauthenticated account."),
        ApiResponse(code = 403, message = "Only a admin can perform this operation."),
        ApiResponse(code = 404, message = "Students not found.")
    ])
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetAllStudents
    fun getAllStudents(
            @RequestParam("page", defaultValue = "0") page: Int,
            @RequestParam("size", defaultValue = "10") size: Int,
    ): List<StudentWithoutPasswordDTO>


    //-------------------------------------------------------------------

    @ApiOperation(value = "Get the details of a single student.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the student with the specified id."),
        ApiResponse(code = 401, message = "Unauthenticated account."),
        ApiResponse(code = 403, message = "Only a admin, reviewer or the actual student can perform this operation.."),
        ApiResponse(code = 404, message = "Student with the given id is not registered in the system.")
    ])
    @GetMapping("/{userID}")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetStudent
    fun getStudentById(@PathVariable userID: Long): StudentWithoutPasswordDTO

    //-------------------------------------------------------------------

    @ApiOperation(value = "Register a new student in the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully registered the student in the system."),
        ApiResponse(code = 201, message = "Successfully registered the student in the system."),
        ApiResponse(code = 401, message = "Unauthenticated account."),
        ApiResponse(code = 403, message = "Only a admin can perform this operation."),
        ApiResponse(code = 404, message = "Resource not found."),
        ApiResponse(code = 409, message = "The student is already registered in the system.")
    ])
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    @AllowForCreateStudent
    fun createStudent(@RequestBody student: StudentDTO): StudentWithoutPasswordDTO

    //-------------------------------------------------------------------

    @ApiOperation(value = "Delete a student from the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully deleted the student from the system."),
        ApiResponse(code = 204, message = "Successfully deleted the student from the system. No content."),
        ApiResponse(code = 401, message = "Unauthenticated account."),
        ApiResponse(code = 403, message = "Only a admin can perform this operation."),
        ApiResponse(code = 404, message = "Student with the given id is not registered in the system.")
    ])
    @DeleteMapping("/{userID}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AllowForDeleteStudent
    fun deleteStudent(@PathVariable userID: Long)

    //-------------------------------------------------------------------
/*
    @ApiOperation(value = "Get all student grant applications with a id from the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the student grant applications with the specified id."),
        ApiResponse(code = 401, message = "Unauthenticated account."),
        ApiResponse(code = 403, message = "Only a admin or the actual student can perform this operation."),
        ApiResponse(code = 404, message = "Student with the given id is not registered in the system.")
    ])
    @GetMapping("/{userID}/grant_applications")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetStudentApplications
    fun getStudentGrantApplications(@PathVariable userID: Long): List<GrantApplicationDTO>
 */

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get all student grant applications with a id by status from the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the student grant applications with the specified id by status."),
        ApiResponse(code = 401, message = "Unauthenticated account."),
        ApiResponse(code = 403, message = "Only a admin or the actual student can perform this operation."),
        ApiResponse(code = 404, message = "Student with the given id is not registered in the system.")
    ])
    @GetMapping("/{userID}/grant_applications")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetStudentApplications
    fun getStudentGrantApplications(@PathVariable userID: Long, @RequestParam("status", required = false) status: GrantApplicationStatus?): List<GrantApplicationForListingDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get all grant calls that are open and do not have a grant application submitted or created by the student with the given id.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all the grant calls submitted or created by the student."),
        ApiResponse(code = 401, message = "Unauthorized request to get all grant calls submitted or created by the student."),
        ApiResponse(code = 403, message = "Forbidden request to get all grant calls submitted or created by the student."),
        ApiResponse(code = 404, message = "GrantCall with the given id is not registered in the system.")
    ])
    @GetMapping("/{studentID}/grant_calls")
    @ResponseStatus(HttpStatus.OK)
    //@AllowForGetGrantCallApplications
    fun getOpenAndNotCreatedGrantCalls(@PathVariable studentID: Long): List<GrantCallDTO>
}
