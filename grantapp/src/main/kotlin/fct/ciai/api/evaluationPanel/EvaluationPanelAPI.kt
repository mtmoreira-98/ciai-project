package fct.ciai.api.evaluationPanel

import fct.ciai.authorization.evaluationPanel.AllowForGetAllGrantApplicationsByEvaluationPanelId
import fct.ciai.authorization.evaluationPanel.AllowForGetAllReviewersByEvaluationPanelId
import fct.ciai.authorization.evaluationPanel.AllowForGetEvaluationPanelById
import fct.ciai.dtos.EvaluationPanelDTO
import fct.ciai.dtos.grantApplications.GrantApplicationDTO
import fct.ciai.dtos.accounts.users.reviewers.ReviewerDTO
import io.swagger.annotations.*
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(value = "Grant Application Management System",
        description= "Manage Operations of Evaluation Panels in the Grant Application System")
@RequestMapping("/evaluation_panels")
interface EvaluationPanelAPI {

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get a specific evaluation panel by ID.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the registered Evaluation panel by ID."),
        ApiResponse(code = 401, message = "Unauthorized evaluation panel get."),
        ApiResponse(code = 403, message = "Forbidden evaluation panel get."),
        ApiResponse(code = 404, message = "Evaluation panel not found.")
    ])
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetEvaluationPanelById
    fun getEvaluationPanelById(@PathVariable id:Long): EvaluationPanelDTO

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get all the registered Reviewers from the Evaluation panel ID.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all the registered Reviewers from the Evaluation panel ID."),
        ApiResponse(code = 401, message = "Unauthorized evaluation panel Reviewers get."),
        ApiResponse(code = 403, message = "Forbidden evaluation panel Reviewers get."),
        ApiResponse(code = 404, message = "Evaluation panel not found.")
    ])
    @GetMapping("/{id}/reviewers")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetAllReviewersByEvaluationPanelId
    fun getAllReviewersByEvaluationPanelId(@PathVariable id:Long): List<ReviewerDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get all the registered Grant Applications from the Evaluation panel ID.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all the registered Grant Applications from the Evaluation panel ID."),
        ApiResponse(code = 401, message = "Unauthorized evaluation panel Grant Applications get."),
        ApiResponse(code = 403, message = "Forbidden evaluation panel Grant Applications get."),
        ApiResponse(code = 404, message = "Evaluation panel not found.")
    ])
    @GetMapping("/{id}/grant_applications")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetAllGrantApplicationsByEvaluationPanelId
    fun getAllGrantApplicationsByEvaluationPanelId(
            @RequestParam("page", defaultValue = "0") page: Int,
            @RequestParam("size", defaultValue = "10") size: Int,
            @PathVariable id: Long): List<GrantApplicationDTO>
}
