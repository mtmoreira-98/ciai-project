package fct.ciai.api.evaluationPanel

import fct.ciai.dtos.EvaluationPanelDTO
import fct.ciai.dtos.grantApplications.GrantApplicationDTO
import fct.ciai.dtos.accounts.users.reviewers.ReviewerDTO
import fct.ciai.services.evaluationPanels.EvaluationPanelService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/evaluation_panels")
class EvaluationPanelController(
        @Autowired val evaluationPanelService: EvaluationPanelService
) : EvaluationPanelAPI {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun getEvaluationPanelById(id: Long): EvaluationPanelDTO {
        logger.info("GET Evaluation Panel by ID")
        return EvaluationPanelDTO(evaluationPanelService.getEvaluationPanelById(id))
    }

    override fun getAllReviewersByEvaluationPanelId(id: Long): List<ReviewerDTO> {
        logger.info("GET Reviewer from Evaluation Panel ID")
        return evaluationPanelService.getAllReviewersByEvaluationPanelId(id).map { ReviewerDTO(it) }
    }

    //TODO: User Stories: 8
    override fun getAllGrantApplicationsByEvaluationPanelId(page: Int, size: Int, id: Long): List<GrantApplicationDTO> {
        logger.info("GET Grant Applications from Evaluation Panel ID")
        return evaluationPanelService.getAllGrantApplicationsByEvaluationPanelId(PageRequest.of(page, size), id).map { GrantApplicationDTO(it) }
    }
}
