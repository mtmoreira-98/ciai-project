package fct.ciai.api.reviews

import fct.ciai.daos.ReviewDAO
import fct.ciai.dtos.FinalReviewDTO
import fct.ciai.dtos.ReviewDTO
import fct.ciai.enums.ReviewStatus
import fct.ciai.services.grants.grantsApplications.GrantApplicationsServices
import fct.ciai.services.reviews.ReviewsServices
import fct.ciai.services.users.reviewers.ReviewersServices
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/reviews")
class ReviewsController(
        @Autowired val reviewsServices: ReviewsServices,
        @Autowired val reviewersServices: ReviewersServices,
        @Autowired val grantApplicationsServices: GrantApplicationsServices
) : ReviewsAPI {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun getAllReviews(page: Int, size: Int): List<ReviewDTO> {
        logger.info("GET ALL Reviews")
        return reviewsServices.getAllReviews(PageRequest.of(page, size)).map { ReviewDTO(it) }
    }

    override fun getReviewById(id: Long): ReviewDTO {
        logger.info("GET Reviews by ID")
        return ReviewDTO(reviewsServices.getReviewById(id))
    }

    //TODO: User Stories: 9
    override fun createReview(review: ReviewDTO) {
        logger.info("CREATE Reviews")
        val reviewerDAO = reviewersServices.getReviewerById(review.reviewerID)
        val grantApplicationDAO = grantApplicationsServices.getGrantApplicationById(review.grantApplicationId)
        val reviewDAO = ReviewDAO(review.id, review.note, review.description, reviewerDAO, grantApplicationDAO, ReviewStatus.NORMAL)
        reviewsServices.addReview(reviewDAO)
    }

    //TODO: User Stories: 11
    override fun createFinalReview(review: FinalReviewDTO) {
        logger.info("CREATE Final Reviews")
        val reviewerDAO = reviewersServices.getReviewerById(review.reviewerID)
        val grantApplicationDAO = grantApplicationsServices.getGrantApplicationById(review.grantApplicationId)
        var status = ReviewStatus.FINAL_ACCEPTED
        if (!review.accepted) {
            status = ReviewStatus.FINAL_REJECTED
        }
        val reviewDAO = ReviewDAO(review.id, review.note, review.description, reviewerDAO, grantApplicationDAO, status)
        reviewsServices.addFinalReview(reviewDAO, review.accepted)
    }

    override fun updateReview(id: Long, review: ReviewDTO) {
        logger.info("UPDATE Reviews")
        val reviewerDAO = reviewersServices.getReviewerById(review.reviewerID)
        val grantApplicationDAO = grantApplicationsServices.getGrantApplicationById(review.grantApplicationId)
        val reviewDAO = ReviewDAO(review.id, review.note, review.description, reviewerDAO, grantApplicationDAO, ReviewStatus.NORMAL)
        reviewsServices.updateReview(id, reviewDAO)
    }

    override fun deleteReview(id: Long) {
        logger.info("DELETE Reviews")
        return reviewsServices.deleteReview(id)
    }

    override fun checkIfAlreadyReviewed(reviewerID: Long, grantApplicationID: Long) {
        reviewsServices.checkIfAlreadyReviewed(reviewerID, grantApplicationID)
    }
}
