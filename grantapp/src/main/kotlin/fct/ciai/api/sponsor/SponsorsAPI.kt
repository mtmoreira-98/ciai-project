package fct.ciai.api.sponsor

import fct.ciai.authorization.sponsors.*
import fct.ciai.dtos.GrantCallDTO
import fct.ciai.dtos.accounts.sponsors.SponsorDTO
import fct.ciai.dtos.accounts.sponsors.SponsorWithoutPasswordDTO
import io.swagger.annotations.*
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(value = "Grant Application Management System",
        description = "Manage Operations of Sponsor in the Grant Application System")
@RequestMapping("/sponsors")
interface SponsorsAPI {

    @ApiOperation(value = "View a list of registered Sponsors.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all the registered Sponsors."),
        ApiResponse(code = 401, message = "Unauthorized Sponsor get."),
        ApiResponse(code = 403, message = "Forbidden Sponsor get."),
        ApiResponse(code = 404, message = "Resource not found.")


    ])
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetAllSponsors
    fun getAllSponsors(
            @RequestParam("page", defaultValue = "0") page: Int,
            @RequestParam("size", defaultValue = "10") size: Int
    ): List<SponsorWithoutPasswordDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "View a specific Sponsor.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned Sponsor."),
        ApiResponse(code = 401, message = "Unauthorized Sponsor get."),
        ApiResponse(code = 403, message = "Forbidden Sponsor get."),
        ApiResponse(code = 404, message = "Resource not found.")
    ])
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetSponsor
    fun getSponsorById(@PathVariable id: Long): SponsorWithoutPasswordDTO

    //-------------------------------------------------------------------

    @ApiOperation(value = "Register a new sponsor in the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully registered the sponsor in the system."),
        ApiResponse(code = 201, message = "Successfully registered the sponsor in the system."),
        ApiResponse(code = 401, message = "Unauthorized registration of the sponsor in the system."),
        ApiResponse(code = 403, message = "Forbidden registration of the sponsor in the system."),
        ApiResponse(code = 404, message = "Resource not found."),
        ApiResponse(code = 409, message = "The sponsor is already registered in the system.")
    ])
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    @AllowForCreateSponsor
    fun createSponsor(@RequestBody sponsor: SponsorDTO)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Delete a  sponsor from the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully deleted the sponsor from the system."),
        ApiResponse(code = 204, message = "Successfully deleted the sponsor from the system. No Content."),
        ApiResponse(code = 401, message = "Unauthorized deletion of the sponsor in the system."),
        ApiResponse(code = 403, message = "Forbidden deletion of the sponsor in the system."),
        ApiResponse(code = 404, message = "Sponsor with the given id is not registered in the system.")
    ])
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AllowForDeleteSponsor
    fun deleteSponsor(@PathVariable id: Long)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get all sponsor's grant calls.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all the sponsor grant calls."),
        ApiResponse(code = 401, message = "Unauthorized sponsor grant calls get."),
        ApiResponse(code = 403, message = "Forbidden sponsor grant calls get."),
        ApiResponse(code = 404, message = "Sponsor with the given id is not registered in the system.")
    ])
    @GetMapping("/{id}/grant_calls")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetSponsorGrantCalls
    fun getSponsorGrantCalls(@PathVariable id: Long): List<GrantCallDTO>

    //-------------------------------------------------------------------
}
