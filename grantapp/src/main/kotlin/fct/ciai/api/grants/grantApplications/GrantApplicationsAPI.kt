package fct.ciai.api.grants.grantApplications

import fct.ciai.authorization.grants.grantApplication.*
import fct.ciai.authorization.reviews.AllowForGetReviewById
import fct.ciai.dtos.grantApplications.GrantApplicationDTO
import fct.ciai.dtos.ReviewDTO
import fct.ciai.dtos.ReviewDetailsDTO
import fct.ciai.dtos.grantApplications.GrantApplicationDetailsDTO
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(value = "Grant Application Management System",
        description= "Manage Operations of GrantCalls in the Grant Application System")
@RequestMapping("/grant_applications")
interface GrantApplicationsAPI {

    //-------------------------------------------------------------------

    @ApiOperation(value = "View a list of registered grant applications.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all the registered grant applications."),
        ApiResponse(code = 401, message = "Unauthorized grant applications get."),
        ApiResponse(code = 403, message = "Forbidden grant applications get."),
        ApiResponse(code = 404, message = "Resource not found.")
    ])
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetAllGrantApplications
    fun getAllGrantApplications(
            @RequestParam("page", defaultValue = "0") page: Int,
            @RequestParam("size", defaultValue = "10") size: Int): List<GrantApplicationDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "View a specific grant application.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the grant application."),
        ApiResponse(code = 401, message = "Unauthorized grant application get."),
        ApiResponse(code = 403, message = "Forbidden grant application get."),
        ApiResponse(code = 404, message = "Grant application not found.")
    ])
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetGrantApplicationById
    fun getGrantApplicationById(@PathVariable id : Long): GrantApplicationDTO

    //-------------------------------------------------------------------

    @ApiOperation(value = "View a specific grant application with more details.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the grant application with more details."),
        ApiResponse(code = 401, message = "Unauthorized grant application get with more details."),
        ApiResponse(code = 403, message = "Forbidden grant application get with more details."),
        ApiResponse(code = 404, message = "Grant application not found.")
    ])
    @GetMapping("/{id}/details")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetGrantApplicationById
    fun getGrantApplicationDetailsById(@PathVariable id: Long): GrantApplicationDetailsDTO

    //-------------------------------------------------------------------

    @ApiOperation(value = "Register a new grant application in the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully registered the grant application in the system."),
        ApiResponse(code = 201, message = "Successfully created the grant application in the system."),
        ApiResponse(code = 400, message = "Bad request in creation on grant application."),
        ApiResponse(code = 401, message = "Unauthorized in creation on grant application."),
        ApiResponse(code = 403, message = "Forbidden in creation on grant application."),
        ApiResponse(code = 404, message = "Resource not found."),
        ApiResponse(code = 409, message = "The grant application is already registered in the system.")
    ])
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    @AllowForCreateGrantApplication
    fun createGrantApplication(@RequestBody grant: GrantApplicationDTO)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Submit a existing grant application in the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully Submited the grant application in the system."),
        ApiResponse(code = 400, message = "Bad request in Submition on grant application."),
        ApiResponse(code = 401, message = "Unauthorized in Submition on grant application."),
        ApiResponse(code = 403, message = "Forbidden in Submition on grant application."),
        ApiResponse(code = 404, message = "Resource not found."),
        ApiResponse(code = 409, message = "The grant application is already Submited in the system.")
    ])
    @PutMapping("/{id}/submit")
    @ResponseStatus(HttpStatus.OK)
    @AllowForCreateGrantApplication
    fun submitGrantApplication(@PathVariable id: Long)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Delete a grant application from the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully deleted the grant application from the system."),
        ApiResponse(code = 204, message = "Successfully deleted the grant application from the system, but no content."),
        ApiResponse(code = 401, message = "Unauthorized operation in deletion of grant application."),
        ApiResponse(code = 403, message = "Forbidden operation in deletion of grant application."),
        ApiResponse(code = 404, message = "Grant application with the given id is not registered in the system.")
    ])
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AllowForDeleteGrantApplication
    fun deleteGrantApplication(@PathVariable id:Long)

    //-------------------------------------------------------------------

    @ApiOperation(value = "List all grant application reviews.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully deleted the grant application from the system."),
        ApiResponse(code = 401, message = "Unauthorized operation in get grant applications reviews get."),
        ApiResponse(code = 403, message = "Forbidden operation in get grant applications reviews."),
        ApiResponse(code = 404, message = "Grant application with the given id is not registered in the system."),
        ApiResponse(code = 423, message = "Grant application reviews locked until final vote.")
    ])
    @GetMapping("/{id}/reviews")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetAllGrantApplicationReviews
    fun getAllGrantApplicationReviews(@PathVariable id:Long) : List<ReviewDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "View a All Reviews with Author.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned All Review."),
        ApiResponse(code = 401, message = "Unauthorized review get."),
        ApiResponse(code = 403, message = "Forbidden review get."),
        ApiResponse(code = 404, message = "Grant Application with the given id is not registered in the system.")
    ])
    @GetMapping("/{id}/reviews_details")
    @ResponseStatus(HttpStatus.OK)
    fun getReviewsWithAuthorByGrantApplicationId(@PathVariable id : Long): List<ReviewDetailsDTO>

}
