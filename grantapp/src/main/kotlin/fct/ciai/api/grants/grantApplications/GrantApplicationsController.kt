package fct.ciai.api.grants.grantApplications

import fct.ciai.daos.DataItemResponseDAO
import fct.ciai.daos.GrantApplicationDAO
import fct.ciai.dtos.grantApplications.GrantApplicationDTO
import fct.ciai.dtos.ReviewDTO
import fct.ciai.dtos.ReviewDetailsDTO
import fct.ciai.dtos.grantApplications.GrantApplicationDetailsDTO
import fct.ciai.services.grants.grantsApplications.GrantApplicationsServices
import fct.ciai.services.grants.grantsCalls.GrantCallsServices
import fct.ciai.services.users.students.StudentsServices
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/grant_applications")
class GrantApplicationsController (
        @Autowired val grantApplicationsService: GrantApplicationsServices,
        @Autowired val grantCallsServices: GrantCallsServices,
        @Autowired val studentsServices: StudentsServices
) : GrantApplicationsAPI {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun getAllGrantApplications(page: Int, size: Int): List<GrantApplicationDTO> {
        logger.info("GET ALL Grant Applications")
        return grantApplicationsService.getAllGrantApplications(PageRequest.of(page, size)).map { GrantApplicationDTO(it) }
    }

    override fun getGrantApplicationById(id: Long): GrantApplicationDTO {
        logger.info("GET Grant Application by ID")
        return GrantApplicationDTO(grantApplicationsService.getGrantApplicationById(id))
    }

    override fun getGrantApplicationDetailsById(id: Long): GrantApplicationDetailsDTO {
        logger.info("GET Grant Application Details by ID")
        //REFECTORY: More efficient - Done in 4 phase
        val it = grantApplicationsService.getGrantApplicationById(id)
        return GrantApplicationDetailsDTO(it, it.grantCall.title, it.student.name)
    }

    //TODO: User Stories: 4
    override fun createGrantApplication(grant: GrantApplicationDTO) {
        logger.info("CREATE Grant Application")
        val pair = getParameters(grant)
        grantApplicationsService.addGrantApplication(pair.first, pair.second)
    }

    override fun submitGrantApplication(id: Long) {
        logger.info("SUBMIT Grant Application")
        grantApplicationsService.submitGrantApplication(id)
    }

    override fun deleteGrantApplication(id: Long) {
        logger.info("DELETE Grant Application")
        grantApplicationsService.deleteGrantApplication(id)
    }

    //TODO: User Stories: 6 / 8
    override fun getAllGrantApplicationReviews(id: Long) : List<ReviewDTO>{
        logger.info("GET ALL Grant Application Reviews by ID")
        return grantApplicationsService.getAllGrantApplicationReviews(id).map { ReviewDTO(it) }
    }



    private fun getParameters(grant: GrantApplicationDTO) : Pair<MutableList<DataItemResponseDAO>, GrantApplicationDAO>{
        val grantCallDAO = grantCallsServices.getGrantCallById(grant.grantCallId)
        val studentDAO = studentsServices.getStudentById(grant.studentID)
        val grantApplicationDAO = GrantApplicationDAO(grant.id, grantCallDAO, studentDAO)
        val dataItemResponsesIds = grant.fieldsResponses.map { it.dataItemId }
        val dataItemResponsesValue = grant.fieldsResponses.map { it.value }
        val dataItemsResponsesDAO = grantCallsServices.verifyDataItemsResponses(grantApplicationDAO, dataItemResponsesIds, dataItemResponsesValue)
        return Pair(dataItemsResponsesDAO, grantApplicationDAO)
    }

    override fun getReviewsWithAuthorByGrantApplicationId(id: Long): List<ReviewDetailsDTO> {
        return grantApplicationsService.getAllGrantApplicationReviews(id).map { ReviewDetailsDTO(it) }
    }

}
