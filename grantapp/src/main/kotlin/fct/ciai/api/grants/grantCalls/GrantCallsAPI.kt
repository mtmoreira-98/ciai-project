package fct.ciai.api.grants.grantCalls

import fct.ciai.authorization.grants.grantCalls.*
import fct.ciai.dtos.grantApplications.GrantApplicationDTO
import fct.ciai.dtos.GrantCallDTO
import fct.ciai.dtos.GrantCallSponsorDTO
import fct.ciai.dtos.grantApplications.GrantApplicationDetailsDTO
import fct.ciai.enums.GrantApplicationStatus
import io.swagger.annotations.*
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(value = "Grant Application Management System",
        description = "Manage Operations of GrantCalls in the Grant Application System")
@RequestMapping("/grant_calls")
interface GrantCallsAPI {


    @ApiOperation(value = "View a specific grant call.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the grant call."),
        ApiResponse(code = 401, message = "Unauthorized request to get grant call."),
        ApiResponse(code = 403, message = "Forbidden request to grant call."),
        ApiResponse(code = 404, message = "Grant Call not found.")
    ])
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    //@AllowForGetGrantCallById
    fun getGrantCallById(@PathVariable id: Long): GrantCallDTO?

    //-------------------------------------------------------------------

    @ApiOperation(value = "Register a new grantCall in the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully registered the grantCall in the system."),
        ApiResponse(code = 201, message = "The grantCall has been created in the system."),
        ApiResponse(code = 401, message = "Unauthorized request to create grant call."),
        ApiResponse(code = 403, message = "Forbidden request to create grant call."),
        ApiResponse(code = 404, message = "Resource not found."),
        ApiResponse(code = 409, message = "The grantCall is already registered in the system.")

    ])
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    @AllowForCreateGrantCall
    fun createGrantCall(@RequestBody grantCall: GrantCallDTO)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Delete a grantCall from the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully deleted the grantCall from the system."),
        ApiResponse(code = 204, message = "Successfully deleted the grantCall from the system, no content."),
        ApiResponse(code = 401, message = "Unauthorized request to delete grant call."),
        ApiResponse(code = 403, message = "Forbidden request to delete grant call."),
        ApiResponse(code = 404, message = "GrantCall with the given id is not registered in the system.")
    ])
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AllowForDeleteGrantCall
    fun deleteGrantCall(@PathVariable id: Long)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get all grant applications to a specified grant call.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all the grant applications from a given grant call."),
        ApiResponse(code = 401, message = "Unauthorized request to get all grant applications to a specified grant call"),
        ApiResponse(code = 403, message = "Forbidden request to get all grant applications to a specified grant call"),
        ApiResponse(code = 404, message = "GrantCall with the given id is not registered in the system.")
    ])
    @GetMapping("/{id}/grant_applications")
    @ResponseStatus(HttpStatus.OK)
    //@AllowForGetGrantCallApplications
    fun getGrantCallApplications(@PathVariable id: Long): List<GrantApplicationDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get a grant call with given status.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the grant calls with given status."),
        ApiResponse(code = 401, message = "Unauthorized request to grant calls."),
        ApiResponse(code = 403, message = "Forbidden request to grant calls."),
        ApiResponse(code = 404, message = "Resource not found.")
    ])
    @ApiParam(name = "status", type = "String", defaultValue = "all")
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    //@AllowForGetAllGrantCalls
    fun getGrantCallsOnStatus(
            @RequestParam("page", defaultValue = "0") page: Int,
            @RequestParam("size", defaultValue = "10") size: Int,
            @RequestParam(defaultValue = "all") status: String): List<GrantCallDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get all closed grant calls.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the closed grant calls."),
        ApiResponse(code = 404, message = "Resource not found.")
    ])
    @GetMapping("/closed")
    @ResponseStatus(HttpStatus.OK)
    fun getClosedGrantCalls(
            @RequestParam("page", defaultValue = "0") page: Int,
            @RequestParam("size", defaultValue = "10") size: Int): List<GrantCallDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get closed grant call accepted application.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the closed grant call accepted application."),
        ApiResponse(code = 404, message = "Resource not found.")
    ])
    @GetMapping("/{id}/accepted_application")
    @ResponseStatus(HttpStatus.OK)
    fun getAcceptedGrantCallApplications(@PathVariable id: Long): List<GrantApplicationDetailsDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "View a specific grant call.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the grant call."),
        ApiResponse(code = 401, message = "Unauthorized request to get grant call."),
        ApiResponse(code = 403, message = "Forbidden request to grant call."),
        ApiResponse(code = 404, message = "Grant Call not found.")
    ])
    @GetMapping("/{id}/complete")
    @ResponseStatus(HttpStatus.OK)
    //@AllowForGetGrantCallById
    fun getGrantCallByIdComplete(@PathVariable id: Long): GrantCallSponsorDTO?

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get a grant call with given status.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the grant calls with given status."),
        ApiResponse(code = 401, message = "Unauthorized request to grant calls."),
        ApiResponse(code = 403, message = "Forbidden request to grant calls."),
        ApiResponse(code = 404, message = "Resource not found.")
    ])
    @ApiParam(name = "status", type = "String", defaultValue = "all")
    @GetMapping("/complete")
    @ResponseStatus(HttpStatus.OK)
    //@AllowForGetAllGrantCalls
    fun getGrantCallsOnStatusComplete(@RequestParam(defaultValue = "all") status: String): List<GrantCallSponsorDTO>


    //-------------------------------------------------------------------

    @ApiOperation(value = "Get all grant applications to a specified grant call by grant application Status.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all the grant applications from a given grant call."),
        ApiResponse(code = 401, message = "Unauthorized request to get all grant applications to a specified grant call"),
        ApiResponse(code = 403, message = "Forbidden request to get all grant applications to a specified grant call"),
        ApiResponse(code = 404, message = "GrantCall with the given id is not registered in the system.")
    ])
    @GetMapping("/{id}/grant_applications_by_status")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetGrantCallApplications
    fun getGrantCallApplicationsByStatus(@PathVariable id: Long, @RequestParam("status", defaultValue = "ACCEPTED") status: GrantApplicationStatus): List<GrantApplicationDetailsDTO>

}
