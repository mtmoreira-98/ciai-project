package fct.ciai.api.institutions

import fct.ciai.daos.InstitutionDAO
import fct.ciai.dtos.InstitutionDTO
import fct.ciai.services.institutions.InstitutionsServices
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/institutions")
class InstitutionsController (
        @Autowired val institutionsServices: InstitutionsServices
) : InstitutionsAPI {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun getAllInstitutions(page: Int, size: Int): List<InstitutionDTO> {
        logger.info("GET ALL Institutions")
        return institutionsServices.getAllInstitutions(PageRequest.of(page, size))
                .map { InstitutionDTO(it) }
    }

    override fun getInstitutionById(id: Long): InstitutionDTO {
        logger.info("GET Institution by ID")
        return InstitutionDTO(institutionsServices.getInstitutionById(id))
    }

    override fun createInstitution(institution: InstitutionDTO) {
        logger.info("CREATE Institution")
        val institutionDAO = InstitutionDAO(
                institution.id,
                institution.name,
                institution.address,
                institution.contact,
                institution.email,
                mutableListOf())
        institutionsServices.addInstitution(institutionDAO)
    }

    override fun deleteInstitution(id: Long) {
        logger.info("DELETE Institution by ID")
        institutionsServices.deleteInstitution(id)
    }
}
