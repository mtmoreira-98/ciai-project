package fct.ciai.api.cv

import fct.ciai.authorization.cv.AllowForCreateCVItem
import fct.ciai.authorization.cv.AllowForDeleteCVItem
import fct.ciai.authorization.cv.AllowForGetCVById
import fct.ciai.authorization.cv.AllowForUpdateCVItem
import fct.ciai.dtos.CVDTO
import fct.ciai.dtos.CVItemDTO
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(value = "Grant Application Management System",
        description= "Manage Operations of CV's in the Grant Application System")
@RequestMapping("/cv")
interface CVAPI {

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get a all CV Items of the CV with the specified ID.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned CV by ID."),
        ApiResponse(code = 401, message = "Unauthorized CV get."),
        ApiResponse(code = 403, message = "Forbidden CV get."),
        ApiResponse(code = 404, message = "CV not found.")
    ])
    @GetMapping("/{cvID}")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetCVById
    fun getCVById(@PathVariable cvID:Long): CVDTO

    //-------------------------------------------------------------------

    @ApiOperation(value = "Create CV item in student CV.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully create CV Item in student CV"),
        ApiResponse(code = 201, message = "Successfully create CV Item in student CV"),
        ApiResponse(code = 401, message = "Unauthorized CV creation."),
        ApiResponse(code = 403, message = "Forbidden CV creation."),
        ApiResponse(code = 404, message = "CV not found.")
    ])
    @PostMapping("/{cvID}/cv_items")
    @ResponseStatus(HttpStatus.CREATED)
    @AllowForCreateCVItem
    fun createCVItem(@PathVariable cvID: Long, @RequestBody cvItem:CVItemDTO)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Update CV item in student CV.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully updated CV by ID."),
        ApiResponse(code = 201, message = "Successfully created a new CV Item."),
        ApiResponse(code = 401, message = "Unauthorized CV update."),
        ApiResponse(code = 403, message = "Forbidden CV updated."),
        ApiResponse(code = 404, message = "CV not found.")
    ])
    @PutMapping("/{cvID}/cv_items/{cvItemID}")
    @ResponseStatus(HttpStatus.OK)
    @AllowForUpdateCVItem
    fun updateCVItem(@PathVariable cvID:Long, @RequestBody cvItem:CVItemDTO, @PathVariable cvItemID:Long)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Delete a CVItem from the CV.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully deleted the CVItem from the CV."),
        ApiResponse(code = 204, message = "Successfully deleted the CVItem from the CV. No content."),
        ApiResponse(code = 401, message = "Unauthorized CVItem delete."),
        ApiResponse(code = 403, message = "Forbidden CVItem delete."),
        ApiResponse(code = 404, message = "CVItem with the given id is not registered in the system.")
    ])
    @DeleteMapping("/{cvID}/cv_items/{cvItemID}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AllowForDeleteCVItem
    fun deleteCVItem(@PathVariable cvID:Long , @PathVariable cvItemID:Long)

    //-------------------------------------------------------------------
}
