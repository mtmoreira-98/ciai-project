package fct.ciai.api.cv

import fct.ciai.daos.CVItemDAO
import fct.ciai.services.cv.CVService
import fct.ciai.dtos.CVDTO
import fct.ciai.dtos.CVItemDTO
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/cv")
class CVController(
        @Autowired val cvService: CVService
) : CVAPI {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun getCVById(cvID: Long): CVDTO {
        logger.info("Get CV by id")
        return CVDTO(cvService.getCVById(cvID))
    }

    override fun createCVItem(cvID: Long, cvItem: CVItemDTO) {
        logger.info("CREATE CV Item")
        val cv = cvService.getCVById(cvID)

        val cVItemDAO = CVItemDAO(cvItem.id, cvItem.type, cvItem.header, cvItem.subTitle, cvItem.value, cv)
        return cvService.createCVItem(cVItemDAO)
    }

    override fun updateCVItem(cvID: Long, cvItem: CVItemDTO, cvItemID: Long) {
        logger.info("UPDATE CV Item")
        return cvService.updateCVItem(cvID, CVItemDAO(cvItem.id, cvItem.type, cvItem.header, cvItem.subTitle, cvItem.value, cvService.getCVById(cvID)))
    }

    override fun deleteCVItem(cvID: Long, cvItemID: Long) {
        logger.info("DELETE CV Item")
        return cvService.deleteCVItem(cvID, cvItemID)
    }
}
