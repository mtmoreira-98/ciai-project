package fct.ciai.sponsorTest

import fct.ciai.dtos.accounts.sponsors.SponsorDTO
import fct.ciai.repositories.sponsor.SponsorRepository
import fct.ciai.services.sponsors.SponsorsServices
import fct.ciai.sponsorTest.SponsorControllerTester.Companion.SPONSOR_URL
import fct.ciai.utils.ObjectsDAO
import fct.ciai.utils.Utils
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class SponsorAuthorizationTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var sponsorRepository: SponsorRepository

    @MockBean
    lateinit var sponsorServices: SponsorsServices

    companion object {

        private val sponsor1DAO = ObjectsDAO.sponsor1DAO

        private val sponsor1DTO = SponsorDTO(sponsor1DAO)

    }

    /*--------------------------------------GET All SPONSORS-------------------------------------------*/

    fun getAll(status: ResultMatcher){
        mvc.perform(get(SPONSOR_URL))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test GET All Sponsors (Not Authenticated)`() {
        getAll(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Sponsors (ADMIN - Authorized)`() {
        getAll(status().isOk)
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET All Sponsors (Sponsor - Not Authorized)`() {
        getAll(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test GET All Sponsors (Student - Not Authorized)`() {
        getAll(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test GET All Sponsors (Reviewer - Not Authorized)`() {
        getAll(status().isForbidden)
    }

    /*---------------------------------------GET SPONSOR By ID---------------------------------------------*/

    fun getById(status: ResultMatcher){
        Mockito.`when`(sponsorRepository.findById(sponsor1DAO.id)).thenReturn(Optional.of(sponsor1DAO))
        Mockito.`when`(sponsorServices.getSponsorById(sponsor1DAO.id)).thenReturn(sponsor1DAO)

        mvc.perform(get("${SPONSOR_URL}/${sponsor1DAO.id}"))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test GET Sponsor By ID (Not Authenticated)`() {
        getById(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Sponsor By ID (ADMIN - Authorized)`() {
        getById(status().isOk)
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Sponsor By ID (Sponsor - Authorized)`() {
        getById(status().isOk)
    }

    @Test
    @WithMockUser(username = "sponsor2", password = "sponsor2", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Sponsor By ID (Sponsor - Not Authorized)`() {
        getById(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test GET Sponsor By ID (Student - Authorized)`() {
        getById(status().isOk)
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test GET Sponsor By ID (Reviewer - Authorized)`() {
        getById(status().isOk)
    }

    /*---------------------------------------CREATE SPONSOR---------------------------------------------*/

    fun create(status: ResultMatcher){

        mvc.perform(post(SPONSOR_URL)
                .contentType(APPLICATION_JSON)
                .content(Utils.toJson(sponsor1DTO)))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test CREATE Sponsor (Not Authenticated)`() {
        create(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Sponsor (ADMIN - Authenticated)`() {
        create(status().isCreated)
    }


    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test CREATE Sponsor (Sponsor - Not Authenticated)`() {
        create(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test CREATE Sponsor (Student - Not Authenticated)`() {
        create(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test CREATE Sponsor (Reviewer - Not Authenticated)`() {
        create(status().isForbidden)
    }

    /*---------------------------------------DELETE SPONSOR---------------------------------------------*/

    fun delete(status: ResultMatcher){
        mvc.perform(delete("${SPONSOR_URL}/${sponsor1DAO.id}"))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test DELETE Sponsor (Not Authenticated)`() {
        delete(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test DELETE Sponsor (ADMIN - Authenticated)`() {
        delete(status().isNoContent)
    }


    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test DELETE Sponsor (Sponsor - Not Authenticated)`() {
        delete(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test DELETE Sponsor (Student - Not Authenticated)`() {
        delete(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test DELETE Sponsor (Reviewer - Not Authenticated)`() {
        delete(status().isForbidden)
    }

    /*---------------------------------------GET SPONSOR Grant Calls---------------------------------------------*/

    fun getGrantCalls(status: ResultMatcher){
        Mockito.`when`(sponsorRepository.findById(sponsor1DAO.id)).thenReturn(Optional.of(sponsor1DAO))
        Mockito.`when`(sponsorServices.getSponsorById(sponsor1DAO.id)).thenReturn(sponsor1DAO)

        mvc.perform(get("${SPONSOR_URL}/${sponsor1DAO.id}/grant_calls"))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test GET Sponsor Grant Calls (Not Authenticated)`() {
        getGrantCalls(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET SPONSOR Grant Calls (ADMIN - Authenticated)`() {
        getGrantCalls(status().isOk)
    }


    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Sponsor Grant Calls (Sponsor - Authenticated)`() {
        getGrantCalls(status().isOk)
    }

    @Test
    @WithMockUser(username = "sponsor2", password = "sponsor2", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Sponsor Grant Calls (Sponsor - Not Authenticated)`() {
        getGrantCalls(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test GET Sponsor Grant Calls (Student - Not Authenticated)`() {
        getGrantCalls(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test GET Sponsor Grant Calls (Reviewer - Not Authenticated)`() {
        getGrantCalls(status().isForbidden)
    }

}
