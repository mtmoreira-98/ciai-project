package fct.ciai.sponsorTest

import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import fct.ciai.dtos.accounts.sponsors.SponsorDTO
import fct.ciai.services.ConflictException
import fct.ciai.services.NotFoundException
import fct.ciai.services.sponsors.SponsorsServices
import fct.ciai.utils.ObjectsDAO
import fct.ciai.utils.Utils
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post


@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class SponsorControllerTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var sponsorsService : SponsorsServices

    companion object {
        const val SPONSOR_URL = "/sponsors"

        private val sponsor1DAO = ObjectsDAO.sponsor1DAO

        private val sponsor1DTO = SponsorDTO(sponsor1DAO)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Sponsors (Non EmptyList)`() {

        Mockito.`when`(sponsorsService.getAllSponsors(PageRequest.of(0, 10))).thenReturn(listOf(sponsor1DAO))

        val result = mvc.perform(get(SPONSOR_URL))
                .andExpect(status().isOk)
                .andReturn()

        val response = Utils.toObject<Iterable<SponsorDTO>>(result)

        Assert.assertEquals(response.toList(), listOf(sponsor1DAO).map { SponsorDTO(it) })
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Sponsors (EmptyList)`() {

        Mockito.`when`(sponsorsService.getAllSponsors(PageRequest.of(0, 10))).thenReturn(emptyList())

        val result = mvc.perform(get(SPONSOR_URL))
                .andExpect(status().isOk)
                .andReturn()

        val response = Utils.toObject<Iterable<SponsorDTO>>(result)
        Assert.assertEquals(response.toList(), emptyList<SponsorDTO>())
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Sponsor by ID (Not Found)`() {

        Mockito.`when`(sponsorsService.getSponsorById(sponsor1DAO.id)).thenThrow(NotFoundException(""))

        mvc.perform(get("$SPONSOR_URL/${sponsor1DTO.id}"))
                .andExpect(status().isNotFound)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Sponsor by ID (Success)`() {

        Mockito.`when`(sponsorsService.getSponsorById(sponsor1DAO.id)).thenReturn(sponsor1DAO)

        val result = mvc.perform(get("$SPONSOR_URL/${sponsor1DAO.id}"))
                .andExpect(status().isOk)
                .andReturn()

        val response = Utils.toObject<SponsorDTO>(result)
        Assert.assertEquals(response, SponsorDTO(sponsor1DAO))
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Sponsor (Conflict)`() {

        Mockito.`when`(sponsorsService.addSponsor(sponsor1DAO)).thenThrow(ConflictException(""))

        mvc.perform(post(SPONSOR_URL)
                .contentType(APPLICATION_JSON)
                .content(Utils.toJson(sponsor1DTO)))
                .andExpect(status().isConflict)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Sponsor (Success)`() {

        Mockito.`when`((sponsorsService).addSponsor(sponsor1DAO)).thenReturn(sponsor1DAO)

        mvc.perform(post(SPONSOR_URL)
                .contentType(APPLICATION_JSON)
                .content(Utils.toJson(sponsor1DTO)))
                .andExpect(status().isCreated)
                .andReturn()
    }

}
