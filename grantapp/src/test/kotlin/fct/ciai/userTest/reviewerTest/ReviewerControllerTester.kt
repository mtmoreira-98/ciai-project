package fct.ciai.userTest.reviewerTest

import fct.ciai.dtos.EvaluationPanelDTO
import fct.ciai.dtos.accounts.users.reviewers.ReviewerDTO
import fct.ciai.dtos.accounts.users.reviewers.ReviewerWithoutPasswordDTO
import fct.ciai.services.MethodNotAllowedException
import fct.ciai.services.NotFoundException
import fct.ciai.services.institutions.InstitutionsServices
import fct.ciai.services.users.reviewers.ReviewersServices
import fct.ciai.utils.ObjectsDAO
import fct.ciai.utils.Utils
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class ReviewerControllerTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var reviewersService : ReviewersServices

    @MockBean
    lateinit var institutionsServices: InstitutionsServices

    companion object {

        const val REVIEWERS_URL = "/reviewers"

        private val emptyList = ObjectsDAO.genericEmptyList

        private val institution1DAO = ObjectsDAO.institution1DAO
        private val reviewer1DAO = ObjectsDAO.reviewer1DAO
        private val evaluationPanelDAO = ObjectsDAO.evaluationPanelDAO

        private val reviewer1DTO = ReviewerDTO(reviewer1DAO)
        private val evaluationPanelDTO = EvaluationPanelDTO(evaluationPanelDAO)

    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Reviewers (Non EmptyList)`() {
        Mockito.`when`(reviewersService.getAllReviewers(PageRequest.of(0, 10))).thenReturn(listOf(reviewer1DAO))

        val result = mvc.perform(get(REVIEWERS_URL))
                .andExpect(status().isOk)
                .andReturn()

        val response = Utils.toObject<Iterable<ReviewerWithoutPasswordDTO>>(result)
        assertEquals(response.toList(), listOf(reviewer1DTO))

    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Reviewers (EmptyList)`() {

        Mockito.`when`(reviewersService.getAllReviewers(PageRequest.of(0, 10))).thenReturn(listOf())

        val result = mvc.perform(get(REVIEWERS_URL))
                .andExpect(status().isOk)
                .andReturn()

        val response = Utils.toObject<Iterable<ReviewerDTO>>(result)

        assertEquals(response.toList(), emptyList)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Reviewer BY Id (Success)`() {

        Mockito.`when`(reviewersService.getReviewerById(reviewer1DAO.id)).thenReturn(reviewer1DAO)

        val result = mvc.perform(get("$REVIEWERS_URL/${reviewer1DAO.id}"))
                .andExpect(status().isOk)
                .andReturn()

        val response = Utils.toObject<ReviewerDTO>(result)
        assertEquals(response.id, reviewer1DTO.id)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Reviewer BY Id (Not Found)`() {

        Mockito.`when`(reviewersService.getReviewerById(reviewer1DAO.id)).thenThrow(NotFoundException(""))

        mvc.perform(get(REVIEWERS_URL + "/${reviewer1DAO.id}"))
                .andExpect(status().isNotFound)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Reviewer (Success)`() {
        Mockito.doNothing().`when`(reviewersService).addReviewer(reviewer1DAO)
        Mockito.`when`(institutionsServices.getInstitutionById(reviewer1DAO.institution.id)).thenReturn(institution1DAO)

        // Convert DTO to JSON
        val ow = Utils.mapper.writer().withDefaultPrettyPrinter()
        val json: String = ow.writeValueAsString(reviewer1DTO)

        mvc.perform(post(REVIEWERS_URL)
                .contentType(APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test DELETE Reviewer (Success)`() {
        Mockito.doNothing().`when`(reviewersService).deleteReviewer(reviewer1DAO.id)

        mvc.perform(delete(REVIEWERS_URL + "/${reviewer1DAO.id}"))
                .andExpect(status().isNoContent)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test DELETE Reviewer (Not Found)`() {
        Mockito.`when`((reviewersService).deleteReviewer(reviewer1DAO.id)).thenThrow(NotFoundException(""))

        mvc.perform(delete(REVIEWERS_URL + "/${reviewer1DAO.id}"))
                .andExpect(status().isNotFound)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test DELETE Reviewer (Not Allowed)`() {
        Mockito.`when`((reviewersService).deleteReviewer(reviewer1DAO.id)).thenThrow(MethodNotAllowedException(""))

        mvc.perform(delete(REVIEWERS_URL + "/${reviewer1DAO.id}"))
                .andExpect(status().isMethodNotAllowed)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Reviewers Evaluations Panels (Not EmptyList)`() {
        Mockito.`when`((reviewersService).getAllReviewerEvaluationPanels(reviewer1DAO.id))
                .thenReturn(listOf(evaluationPanelDAO))

        val result = mvc.perform(get(REVIEWERS_URL + "/${reviewer1DAO.id}/evaluations_panels"))
                .andExpect(status().isOk)
                .andReturn()

        val response = Utils.toObject<Iterable<EvaluationPanelDTO>>(result)
        assertEquals(response, mutableListOf(evaluationPanelDTO))
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Reviewers Evaluations Panels (EmptyList)`() {
        Mockito.`when`((reviewersService).getAllReviewerEvaluationPanels(reviewer1DAO.id))
                .thenReturn(mutableListOf())

        val result = mvc.perform(get(REVIEWERS_URL + "/${evaluationPanelDAO.id}/evaluations_panels"))
                .andExpect(status().isOk)
                .andReturn()

        val response = Utils.toObject<Iterable<EvaluationPanelDTO>>(result)
        assertEquals(response, emptyList)
    }
}
