package fct.ciai.userTest.studentTest

import fct.ciai.dtos.accounts.users.students.StudentDTO
import fct.ciai.repositories.users.students.StudentRepository
import fct.ciai.services.institutions.InstitutionsServices
import fct.ciai.services.users.students.StudentsServices
import fct.ciai.userTest.studentTest.StudentControllerTester.Companion.STUDENTS_URL
import fct.ciai.utils.ObjectsDAO
import fct.ciai.utils.Utils
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*


@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class StudentAuthorizationTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var studentRepository: StudentRepository

    @MockBean
    lateinit var studentsServices: StudentsServices

    @MockBean
    lateinit var institutionsServices: InstitutionsServices

    companion object {

        private val student1DAO = ObjectsDAO.student1DAO

        private val institution1DAO = ObjectsDAO.institution1DAO

        private val student1DTO = StudentDTO(student1DAO)

    }

    /*-------------------------------------GET All STUDENTS-------------------------------------------*/
    fun getAll(status: ResultMatcher){
        mvc.perform(get(STUDENTS_URL))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test GET All Students (Not Authenticated)`() {
        getAll(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Students (ADMIN - Authorized)`() {
        getAll(status().isOk)
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET All Students (Sponsor - Not Authorized)`() {
        getAll(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test GET All Students (Student - Not Authorized)`() {
        getAll(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test GET All Students (Reviewer - Not Authorized)`() {
        getAll(status().isForbidden)
    }

    /*------------------------------------GET STUDENT BY ID-------------------------------------------*/
    fun getById(status: ResultMatcher){
        Mockito.`when`(studentRepository.findById(student1DAO.id)).thenReturn(Optional.of(student1DAO))
        Mockito.`when`(studentsServices.getStudentById(student1DAO.id)).thenReturn(student1DAO)

        mvc.perform(get("${STUDENTS_URL}/${student1DAO.id}"))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test GET Student By ID (Not Authenticated)`() {
        getById(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Student By ID (ADMIN - Authorized)`() {
        getById(status().isOk)
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Student By ID (Student - Authorized)`() {
        getById(status().isOk)
    }

    @Test
    @WithMockUser(username = "student2", password = "student2", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Student By ID (Student - Not Authorized)`() {
        getById(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_STUDENT"])
    fun `Test GET Student By ID (Sponsor - Not Authorized)`() {
        getById(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test GET Student By ID (Reviewer - Not Authorized)`() {
        getById(status().isOk)
    }

    /*--------------------------------------CREATE STUDENT--------------------------------------------*/

    fun create(status: ResultMatcher){

        Mockito.`when`(institutionsServices.getInstitutionById(institution1DAO.id)).thenReturn(institution1DAO)
        Mockito.`when`(studentsServices.addStudent(student1DAO)).thenReturn(student1DAO)

        mvc.perform(post(STUDENTS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(student1DTO)))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test CREATE Student (Not Authenticated)`() {
        create(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Student (ADMIN - Authenticated)`() {
        create(status().isCreated)
    }


    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test CREATE Student (Sponsor - Not Authenticated)`() {
        create(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test CREATE Student (Student - Not Authenticated)`() {
        create(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test CREATE Student (Reviewer - Not Authenticated)`() {
        create(status().isForbidden)
    }

    /*--------------------------------------DELETE STUDENT--------------------------------------------*/
    fun delete(status: ResultMatcher){
        mvc.perform(delete("${STUDENTS_URL}/${student1DAO.id}"))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test DELETE Student (Not Authenticated)`() {
        delete(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test DELETE Student (ADMIN - Authenticated)`() {
        delete(status().isNoContent)
    }


    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test DELETE Student (Sponsor - Not Authenticated)`() {
        delete(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test DELETE Student (Student - Not Authenticated)`() {
        delete(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test DELETE Student (Reviewer - Not Authenticated)`() {
        delete(status().isForbidden)
    }

    /*---------------------------------GET STUDENT GRANT APPLICATIONS---------------------------------*/

    fun getGrantCalls(status: ResultMatcher){
        Mockito.`when`(studentRepository.findById(student1DAO.id)).thenReturn(Optional.of(student1DAO))
        Mockito.`when`(studentsServices.getStudentById(student1DAO.id)).thenReturn(student1DAO)

        mvc.perform(get("${STUDENTS_URL}/${student1DAO.id}/grant_applications"))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test GET Students Grant Applications (Not Authenticated)`() {
        getGrantCalls(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Students Grant Applications (ADMIN - Authenticated)`() {
        getGrantCalls(status().isOk)
    }


    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Students Grant Applications (Student - Authenticated)`() {
        getGrantCalls(status().isOk)
    }

    @Test
    @WithMockUser(username = "student2", password = "student2", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Students Grant Applications (Student - Not Authenticated)`() {
        getGrantCalls(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_STUDENT"])
    fun `Test GET Students Grant Applications (Sponsor - Not Authenticated)`() {
        getGrantCalls(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test GET Students Grant Applications (Reviewer - Not Authenticated)`() {
        getGrantCalls(status().isForbidden)
    }

}
