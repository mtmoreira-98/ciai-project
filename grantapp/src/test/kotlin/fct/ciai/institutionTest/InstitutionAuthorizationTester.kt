package fct.ciai.institutionTest

import fct.ciai.dtos.InstitutionDTO
import fct.ciai.institutionTest.InstitutionControllerTester.Companion.INSTITUTIONS_URL
import fct.ciai.repositories.institutions.InstitutionRepository
import fct.ciai.services.institutions.InstitutionsServices
import fct.ciai.utils.ObjectsDAO
import fct.ciai.utils.Utils
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*


@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class InstitutionAuthorizationTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var institutionRepository: InstitutionRepository

    @MockBean
    lateinit var institutionServices: InstitutionsServices

    companion object {

        private val institution1DAO = ObjectsDAO.institution1DAO

        private val institution1DTO = InstitutionDTO(ObjectsDAO.institution1DAO)

    }

    /*getAllInstitutions*/
    fun getAllInstitutions(status: ResultMatcher){
        mvc.perform(get(INSTITUTIONS_URL))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test GET All Institutions (Not Authenticated)`() {
        getAllInstitutions(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Institutions (ADMIN - Authorized)`() {
        getAllInstitutions(status().isOk)
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET All Institutions (Sponsor - Authorized)`() {
        getAllInstitutions(status().isOk)
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test GET All Institutions (Student - Authorized)`() {
        getAllInstitutions(status().isOk)
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test GET All Institutions (Reviewer - Authorized)`() {
        getAllInstitutions(status().isOk)
    }

    /*getInstitutionById*/
    fun getById(status: ResultMatcher){
        Mockito.`when`(institutionRepository.findById(institution1DAO.id)).thenReturn(Optional.of(institution1DAO))
        Mockito.`when`(institutionServices.getInstitutionById(institution1DAO.id)).thenReturn(institution1DAO)

        mvc.perform(get("${INSTITUTIONS_URL}/${institution1DTO.id}"))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test GET Institutions by ID (Not Authenticated)`() {
        getById(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Institutions by ID (ADMIN - Authorized)`() {
        getById(status().isOk)
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Institutions by ID (Sponsor - Authorized)`() {
        getById(status().isOk)
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test GET Institutions by ID (Student - Authorized)`() {
        getById(status().isOk)
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test GET Institutions by ID (Reviewer - Authorized)`() {
        getById(status().isOk)
    }

    /*createInstitution*/
    fun create(status: ResultMatcher){
        mvc.perform(post(INSTITUTIONS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(institution1DTO)))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test CREATE Institutions (Not Authenticated)`() {
        create(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Institutions (ADMIN - Authorized)`() {
        create(status().isCreated)
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test CREATE Institutions (Sponsor - Not Authorized)`() {
        create(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test CREATE Institutions (Student - Not Authorized)`() {
        create(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test CREATE Institutions (Reviewer - Not Authorized)`() {
        create(status().isForbidden)
    }

    /*deleteInstitution*/
    fun delete(status: ResultMatcher){
        mvc.perform(delete("${INSTITUTIONS_URL}/${institution1DTO.id}"))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test DELETE Institutions (Not Authenticated)`() {
        delete(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test DELETE Institutions (ADMIN - Authorized)`() {
        delete(status().isNoContent)
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test DELETE Institutions (Sponsor - Not Authorized)`() {
        delete(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test DELETE Institutions (Student - Not Authorized)`() {
        delete(status().isForbidden)
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test DELETE Institutions (Reviewer - Not Authorized)`() {
        delete(status().isForbidden)
    }

}

