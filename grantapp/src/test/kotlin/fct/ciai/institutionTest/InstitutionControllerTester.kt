package fct.ciai.institutionTest

import fct.ciai.daos.InstitutionDAO
import fct.ciai.dtos.InstitutionDTO
import fct.ciai.services.ConflictException
import fct.ciai.services.NotFoundException
import fct.ciai.services.institutions.InstitutionsServices
import fct.ciai.utils.ObjectsDAO
import fct.ciai.utils.Utils
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class InstitutionControllerTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var institutionService : InstitutionsServices

    companion object{
        const val INSTITUTIONS_URL = "/institutions"

        private val emptyList = ObjectsDAO.getEmptyList<InstitutionDAO>()

        private val institution1DAO = ObjectsDAO.institution1DAO
        private val institution2DAO = ObjectsDAO.institution2DAO

        private val institution1DTO = InstitutionDTO(institution1DAO)
        private val institution2DTO = InstitutionDTO(institution2DAO)

    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Institution`() {
        Mockito.`when`(institutionService.getInstitutionById(institution1DAO.id)).thenReturn(institution1DAO)

        val result = mvc.perform(get("$INSTITUTIONS_URL/${institution1DAO.id}"))
                .andExpect(status().isOk)
                .andReturn()

        val responseDTO = Utils.toObject<InstitutionDTO>(result)
        assertEquals(responseDTO, institution1DTO)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Institution (Not Found)`() {
        Mockito.`when`(institutionService.getInstitutionById(institution1DAO.id)).thenThrow(NotFoundException(""))
        mvc.perform(get("$INSTITUTIONS_URL/${institution1DAO.id}"))
                .andExpect(status().isNotFound)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Institutions (EmptyList)`() {
        Mockito.`when`(institutionService.getAllInstitutions(PageRequest.of(0, 10))).thenReturn(emptyList)

        val result = mvc.perform(get(INSTITUTIONS_URL))
                .andExpect(status().isOk)
                .andReturn()

        val responseList = Utils.toObject<Iterable<InstitutionDTO>>(result)
        assertEquals(responseList, emptyList)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Institutions (Non EmptyList)`() {
        Mockito.`when`(institutionService.getAllInstitutions(PageRequest.of(0, 10)))
                .thenReturn(listOf(institution1DAO, institution2DAO))

        val result = mvc.perform(get(INSTITUTIONS_URL))
                .andExpect(status().isOk)
                .andReturn()

        val responseList = Utils.toObject<Iterable<InstitutionDTO>>(result)
        assertEquals(responseList.toList(), listOf(institution1DTO, institution2DTO))
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Institution (Conflict Error)`() {
        Mockito.`when`(institutionService.addInstitution(institution1DAO))
                .thenThrow(ConflictException(""))

        mvc.perform(post(INSTITUTIONS_URL)
                .contentType(APPLICATION_JSON)
                .content(Utils.toJson(institution1DTO)))
                .andExpect(status().isConflict)
                .andReturn()
    }
}
