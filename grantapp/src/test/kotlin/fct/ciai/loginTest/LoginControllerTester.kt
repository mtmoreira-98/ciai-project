package fct.ciai.institutionTest

import fct.ciai.daos.AccountDAO
import fct.ciai.daos.InstitutionDAO
import fct.ciai.daos.StudentDAO
import fct.ciai.dtos.InstitutionDTO
import fct.ciai.dtos.LoginFormDTO
import fct.ciai.dtos.LoginResponseDTO
import fct.ciai.dtos.ReviewDTO
import fct.ciai.dtos.accounts.AccountDTO
import fct.ciai.dtos.accounts.users.reviewers.ReviewerDTO
import fct.ciai.dtos.accounts.users.students.StudentDTO
import fct.ciai.enums.RolesEnum
import fct.ciai.institutionTest.InstitutionControllerTester.Companion.INSTITUTIONS_URL
import fct.ciai.repositories.institutions.InstitutionRepository
import fct.ciai.services.ForbiddenException
import fct.ciai.services.institutions.InstitutionsServices
import fct.ciai.services.users.AccountsService
import fct.ciai.userTest.studentTest.StudentControllerTester
import fct.ciai.utils.ObjectsDAO
import fct.ciai.utils.Utils
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*


@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class LoginControllerTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var accountsService: AccountsService

    companion object{
        const val LOGIN_URL = "/login"

        private val accountStudentRoleDAO = ObjectsDAO.accountStudentRoleDAO
        private val accountReviewerRoleDAO = ObjectsDAO.accountReviewerRoleDAO

        private val accountStudentRoleDTO = AccountDTO(accountStudentRoleDAO)
        private val accountReviewerRoleDTO = AccountDTO(accountReviewerRoleDAO)

    }

    /*Login*/
    fun loginRequest(account: AccountDAO, status: ResultMatcher) : MvcResult {
        return mvc.perform(post(LOGIN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(LoginFormDTO(account.userName, "password"))))
                .andExpect(status)
                .andReturn()
    }

    @Test
    fun `Test Student Login (Success)`() {
        Mockito.`when`(accountsService.login(accountStudentRoleDTO.userName, "password"))
                .thenReturn(accountStudentRoleDAO)

        val result = loginRequest(accountStudentRoleDAO, status().isOk)

        val response = Utils.toObject<LoginResponseDTO>(result)
        Assert.assertEquals(response.accountRoles.get(0), RolesEnum.ROLE_STUDENT.toString())
    }

    @Test
    fun `Test Reviewer Login (Success)`() {
        Mockito.`when`(accountsService.login(accountReviewerRoleDAO.userName, "password"))
                .thenReturn(accountReviewerRoleDAO)

        val result = loginRequest(accountReviewerRoleDAO, status().isOk)

        val response = Utils.toObject<LoginResponseDTO>(result)
        Assert.assertEquals(response.accountRoles.get(0), RolesEnum.ROLE_REVIEWER.toString())
    }

    @Test
    fun `Test Login (Forbidden)`() {
        Mockito.`when`(accountsService.login(accountReviewerRoleDTO.userName, "password"))
                .thenThrow(ForbiddenException(""))

        loginRequest(accountReviewerRoleDAO, status().isForbidden)
    }

}

