package fct.ciai.grantsTests.grantCallTest

import fct.ciai.dtos.grantApplications.GrantApplicationDTO
import fct.ciai.grantsTests.grantApplicationTest.GrantApplicationControllerTester.Companion.GRANT_APPLICATIONS_URL
import fct.ciai.repositories.grants.GrantApplicationRepository
import fct.ciai.repositories.users.AccountRepository
import fct.ciai.services.grants.grantsApplications.GrantApplicationsServices
import fct.ciai.services.grants.grantsCalls.GrantCallsServices
import fct.ciai.services.users.students.StudentsServices
import fct.ciai.utils.ObjectsDAO
import fct.ciai.utils.Utils
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class GrantApplicationAuthorizationTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var accountRepository: AccountRepository

    @MockBean
    lateinit var grantApplicationRepository: GrantApplicationRepository

    @MockBean
    lateinit var grantApplicationsService: GrantApplicationsServices

    @MockBean
    lateinit var grantCallsServices: GrantCallsServices

    @MockBean
    lateinit var studentsServices: StudentsServices

    companion object {
        private val reviewer1DAO = ObjectsDAO.reviewer1DAO
        private val reviewer2DAO = ObjectsDAO.reviewer2DAO
        private val sponsor1DAO = ObjectsDAO.sponsor1DAO
        private val sponsor2DAO = ObjectsDAO.sponsor2DAO
        private val student1DAO = ObjectsDAO.student1DAO
        private val student2DAO = ObjectsDAO.student2DAO
        private val grantApplicationDAO = ObjectsDAO.grantApplicationWithReviewer1DAO

        private val grantApplicationDTO = GrantApplicationDTO(grantApplicationDAO)

    }

    /*Get Grant Application By Id Test*/
    @Test
    fun `Test GET Grant Application By Id (Not Authenticated)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)
        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}"))
                .andExpect(status().isUnauthorized)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Grant Application By Id (ADMIN - Authorized)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)
        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test GET Grant Application By Id (STUDENT - Authorized)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)

        //Used in GrantApplicationSecurityService
        Mockito.`when`(accountRepository.findByUserName(student1DAO.userName)).thenReturn(Optional.of(student1DAO))
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id)).thenReturn(Optional.of(grantApplicationDAO))

        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test GET Grant Application By Id (REVIEWER - Authorized)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)

        //Used in GrantApplicationSecurityService
        Mockito.`when`(accountRepository.findByUserName(reviewer1DAO.userName)).thenReturn(Optional.of(reviewer1DAO))
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id)).thenReturn(Optional.of(grantApplicationDAO))

        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Grant Application By Id (SPONSOR - Authorized)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)

        //Used in GrantApplicationSecurityService
        Mockito.`when`(accountRepository.findByUserName(sponsor1DAO.userName)).thenReturn(Optional.of(sponsor1DAO))
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id)).thenReturn(Optional.of(grantApplicationDAO))

        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student2", password = "student2", authorities = ["ROLE_STUDENT"])
    fun `Test GET Grant Application By Id (STUDENT - Not Authorized)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)

        //Used in GrantApplicationSecurityService
        Mockito.`when`(accountRepository.findByUserName(student2DAO.userName)).thenReturn(Optional.of(student2DAO))
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id)).thenReturn(Optional.of(grantApplicationDAO))

        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}"))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer2", password = "reviewer2", authorities = ["ROLE_REVIEWER"])
    fun `Test GET Grant Application By Id (REVIEWER - Not Authorized)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)

        //Used in GrantApplicationSecurityService
        Mockito.`when`(accountRepository.findByUserName(reviewer2DAO.userName)).thenReturn(Optional.of(reviewer2DAO))
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id)).thenReturn(Optional.of(grantApplicationDAO))

        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}"))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor2", password = "sponsor2", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Grant Application By Id (SPONSOR - Not Authorized)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)

        //Used in GrantApplicationSecurityService
        Mockito.`when`(accountRepository.findByUserName(sponsor2DAO.userName)).thenReturn(Optional.of(sponsor2DAO))
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id)).thenReturn(Optional.of(grantApplicationDAO))

        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}"))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    /*Get ALL GrantCalls Test*/
    @Test
    fun `Test GET All GrantCalls (Not Authenticated)`() {
        mvc.perform(get(GRANT_APPLICATIONS_URL))
                .andExpect(status().isUnauthorized)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All GrantCalls (ADMIN - Authorized)`() {
        mvc.perform(get(GRANT_APPLICATIONS_URL))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET All GrantCalls (SPONSOR -Not Authorized)`() {
        mvc.perform(get(GRANT_APPLICATIONS_URL))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test GET All GrantCalls (STUDENT - Not Authorized)`() {
        mvc.perform(get(GRANT_APPLICATIONS_URL))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test GET All GrantCalls (REVIEWER - Not Authorized)`() {
        mvc.perform(get(GRANT_APPLICATIONS_URL))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    /*CREATE Grant Application Test*/
    @Test
    fun `Test CREATE Grant Application  (ADMIN - Not Authenticated)`() {
        mvc.perform(post(GRANT_APPLICATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(grantApplicationDTO)))
                .andExpect(status().isUnauthorized)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Grant Application (ADMIN - Authorized)`() {
        Mockito.`when`(grantCallsServices.getGrantCallById(grantApplicationDAO.grantCall.id)).thenReturn(grantApplicationDAO.grantCall)
        Mockito.`when`(studentsServices.getStudentById(grantApplicationDAO.student.id)).thenReturn(grantApplicationDAO.student)
        val dataItems = grantApplicationDAO.fieldsResponses.map { it.dataItem.id }
        val values = grantApplicationDAO.fieldsResponses.map { it.value }
        Mockito.`when`(grantCallsServices.verifyDataItemsResponses(grantApplicationDAO, dataItems,values)).thenReturn(grantApplicationDAO.fieldsResponses)
        Mockito.doNothing().`when`(grantApplicationsService).addGrantApplication(grantApplicationDAO.fieldsResponses, grantApplicationDAO)

        mvc.perform(post(GRANT_APPLICATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(grantApplicationDTO)))
                .andExpect(status().isCreated)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test CREATE Grant Application (STUDENT - Authorized)`() {
        Mockito.`when`(grantCallsServices.getGrantCallById(grantApplicationDAO.grantCall.id)).thenReturn(grantApplicationDAO.grantCall)
        Mockito.`when`(studentsServices.getStudentById(grantApplicationDAO.student.id)).thenReturn(grantApplicationDAO.student)
        val dataItems = grantApplicationDAO.fieldsResponses.map { it.dataItem.id }
        val values = grantApplicationDAO.fieldsResponses.map { it.value }
        Mockito.`when`(grantCallsServices.verifyDataItemsResponses(grantApplicationDAO, dataItems, values)).thenReturn(grantApplicationDAO.fieldsResponses)
        Mockito.doNothing().`when`(grantApplicationsService).addGrantApplication(grantApplicationDAO.fieldsResponses, grantApplicationDAO)

        mvc.perform(post(GRANT_APPLICATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(grantApplicationDTO)))
                .andExpect(status().isCreated)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test CREATE Grant Application REVIEWER - Not Authorized)`() {
        mvc.perform(post(GRANT_APPLICATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(grantApplicationDTO)))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test CREATE Grant Application (SPONSOR - Authorized)`() {
        mvc.perform(post(GRANT_APPLICATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(grantApplicationDTO)))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    // Test Get GrantCall Reviews

    @Test
    fun `Test GET Grant Application Reviews (Not Authenticated)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)
        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}/reviews"))
                .andExpect(status().isUnauthorized)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Grant Application Reviews (ADMIN - Authorized)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)
        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}/reviews"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test GET Grant Application Reviews (STUDENT - Authorized)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)

        //Used in GrantApplicationSecurityService
        Mockito.`when`(accountRepository.findByUserName(student1DAO.userName)).thenReturn(Optional.of(student1DAO))
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id)).thenReturn(Optional.of(grantApplicationDAO))

        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}/reviews"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test GET Grant Application Reviews (REVIEWER - Authorized)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)

        //Used in GrantApplicationSecurityService
        Mockito.`when`(accountRepository.findByUserName(reviewer1DAO.userName)).thenReturn(Optional.of(reviewer1DAO))
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id)).thenReturn(Optional.of(grantApplicationDAO))

        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}/reviews"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Grant Application Reviews (SPONSOR - Authorized)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)

        //Used in GrantApplicationSecurityService
        Mockito.`when`(accountRepository.findByUserName(sponsor1DAO.userName)).thenReturn(Optional.of(sponsor1DAO))
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id)).thenReturn(Optional.of(grantApplicationDAO))

        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}/reviews"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student2", password = "student2", authorities = ["ROLE_STUDENT"])
    fun `Test GET Grant Application Reviews (STUDENT - Not Authorized)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)

        //Used in GrantApplicationSecurityService
        Mockito.`when`(accountRepository.findByUserName(student2DAO.userName)).thenReturn(Optional.of(student2DAO))
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id)).thenReturn(Optional.of(grantApplicationDAO))

        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}/reviews"))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer2", password = "reviewer2", authorities = ["ROLE_REVIEWER"])
    fun `Test GET Grant Application Reviews (REVIEWER - Not Authorized)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)

        //Used in GrantApplicationSecurityService
        Mockito.`when`(accountRepository.findByUserName(reviewer2DAO.userName)).thenReturn(Optional.of(reviewer2DAO))
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id)).thenReturn(Optional.of(grantApplicationDAO))

        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}/reviews"))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor2", password = "sponsor2", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Grant Application Reviews (SPONSOR - Not Authorized)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id)).thenReturn(grantApplicationDAO)

        //Used in GrantApplicationSecurityService
        Mockito.`when`(accountRepository.findByUserName(sponsor2DAO.userName)).thenReturn(Optional.of(sponsor2DAO))
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id)).thenReturn(Optional.of(grantApplicationDAO))

        mvc.perform(get("$GRANT_APPLICATIONS_URL/${grantApplicationDAO.id}/reviews"))
                .andExpect(status().isForbidden)
                .andReturn()
    }

}
