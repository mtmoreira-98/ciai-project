package fct.ciai.grantsTests.grantCallTest

import fct.ciai.dtos.grantApplications.GrantApplicationDTO
import fct.ciai.dtos.GrantCallDTO
import fct.ciai.services.NotFoundException
import fct.ciai.services.grants.grantsCalls.GrantCallsServices
import fct.ciai.services.sponsors.SponsorsServices
import fct.ciai.services.users.reviewers.ReviewersServices
import fct.ciai.utils.ObjectsDAO
import fct.ciai.utils.Utils
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class GrantCallControllerTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var grantCallsServices: GrantCallsServices

    @MockBean
    lateinit var sponsorsServices: SponsorsServices

    @MockBean
    lateinit var reviewersServices: ReviewersServices


    companion object {
        const val GRANT_CALLS_URL = "/grant_calls"

        private val emptyList = ObjectsDAO.genericEmptyList

        private val grantCallDAO = ObjectsDAO.grantCallDAO
        private val grantApplicationDAO = ObjectsDAO.grantApplicationDAO

        private val grantCallDTO = GrantCallDTO(grantCallDAO)
        private val grantApplicationDTO = GrantApplicationDTO(grantApplicationDAO)

    }

    /*getAllGrantCalls*/

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Grant Calls (Not Empty List)`() {
        Mockito.`when`(grantCallsServices.getAllGrantOnStatus("all"))
                .thenReturn(listOf(grantCallDAO))

        val result = mvc.perform(get("$GRANT_CALLS_URL"))
                .andExpect(status().isOk)
                .andReturn()

        val response = Utils.toObject<Iterable<GrantCallDTO>>(result)
        assertEquals(response, listOf(grantCallDTO) )
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Grant Calls (Empty List)`() {
        Mockito.`when`(grantCallsServices.getAllGrantOnStatus("all"))
                .thenReturn(listOf())

        val result = mvc.perform(get("$GRANT_CALLS_URL?status=all"))
                .andExpect(status().isOk)
                .andReturn()

        val response = Utils.toObject<Iterable<GrantCallDTO>>(result)
        assertEquals(response, emptyList)
    }

    /*getGrantCallById*/

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Grant Calls By ID (Success)`() {
        Mockito.`when`(grantCallsServices.getGrantCallById(grantCallDAO.id))
                .thenReturn(grantCallDAO)

        val result = mvc.perform(get("$GRANT_CALLS_URL/${grantCallDAO.id}"))
                .andExpect(status().isOk)
                .andReturn()

        val response = Utils.toObject<GrantCallDTO>(result)
        assertEquals(response, grantCallDTO)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Grant Calls By ID (Not Exists)`() {
        Mockito.`when`(grantCallsServices.getGrantCallById(grantCallDAO.id))
                .thenThrow(NotFoundException(""))

        mvc.perform(get("$GRANT_CALLS_URL/${grantCallDAO.id}"))
                .andExpect(status().isNotFound)
                .andReturn()

    }

    /*createGrantCall*/

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Grant Calls (Success)`() {
        Mockito.`when`(sponsorsServices.getSponsorById(grantCallDTO.sponsorId)).thenReturn(grantCallDAO.sponsor)
        Mockito.`when`(reviewersServices.getReviewerById(grantCallDTO.evaluationPanel.panelChair)).thenReturn(grantCallDAO.evaluationPanel.panelChair)
        Mockito.`when`(reviewersServices.getReviewersById(grantCallDTO.evaluationPanel.reviewersIDs)).thenReturn(grantCallDAO.evaluationPanel.panelReviewers)
        Mockito.doNothing().`when`(grantCallsServices).addGrantCall(grantCallDAO, grantCallDAO.dataItems, grantCallDAO.evaluationPanel)

        mvc.perform(post(GRANT_CALLS_URL)
                .contentType(APPLICATION_JSON)
                .content(Utils.toJson(grantCallDTO)))
                .andExpect(status().isCreated)
                .andReturn()

    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Grant Calls (Sponsor not exists)`() {
        Mockito.`when`(sponsorsServices.getSponsorById(grantCallDTO.sponsorId)).thenThrow(NotFoundException(""))
        Mockito.`when`(reviewersServices.getReviewerById(grantCallDTO.evaluationPanel.panelChair)).thenReturn(grantCallDAO.evaluationPanel.panelChair)
        Mockito.`when`(reviewersServices.getReviewersById(grantCallDTO.evaluationPanel.reviewersIDs)).thenReturn(grantCallDAO.evaluationPanel.panelReviewers)
        Mockito.doNothing().`when`(grantCallsServices).addGrantCall(grantCallDAO, grantCallDAO.dataItems, grantCallDAO.evaluationPanel)

        mvc.perform(post(GRANT_CALLS_URL)
                .contentType(APPLICATION_JSON)
                .content(Utils.toJson(grantCallDTO)))
                .andExpect(status().isNotFound)
                .andReturn()

    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Grant Calls (PanelChair not exists)`() {
        Mockito.`when`(sponsorsServices.getSponsorById(grantCallDTO.sponsorId)).thenReturn(grantCallDAO.sponsor)
        Mockito.`when`(reviewersServices.getReviewerById(grantCallDTO.evaluationPanel.panelChair)).thenThrow(NotFoundException(""))
        Mockito.`when`(reviewersServices.getReviewersById(grantCallDTO.evaluationPanel.reviewersIDs)).thenReturn(grantCallDAO.evaluationPanel.panelReviewers)
        Mockito.doNothing().`when`(grantCallsServices).addGrantCall(grantCallDAO, grantCallDAO.dataItems, grantCallDAO.evaluationPanel)

        mvc.perform(post(GRANT_CALLS_URL)
                .contentType(APPLICATION_JSON)
                .content(Utils.toJson(grantCallDTO)))
                .andExpect(status().isNotFound)
                .andReturn()

    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Grant Calls (Reviewer not exists)`() {
        Mockito.`when`(sponsorsServices.getSponsorById(grantCallDTO.sponsorId)).thenReturn(grantCallDAO.sponsor)
        Mockito.`when`(reviewersServices.getReviewerById(grantCallDTO.evaluationPanel.panelChair)).thenReturn(grantCallDAO.evaluationPanel.panelChair)
        Mockito.`when`(reviewersServices.getReviewersById(grantCallDTO.evaluationPanel.reviewersIDs)).thenThrow(NotFoundException(""))
        Mockito.doNothing().`when`(grantCallsServices).addGrantCall(grantCallDAO, grantCallDAO.dataItems, grantCallDAO.evaluationPanel)

        mvc.perform(post(GRANT_CALLS_URL)
                .contentType(APPLICATION_JSON)
                .content(Utils.toJson(grantCallDTO)))
                .andExpect(status().isNotFound)
                .andReturn()

    }

    /*deleteGrantCall*/

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test DELETE Grant Calls (Success)`() {
        Mockito.doNothing().`when`(grantCallsServices).deleteGrantCall(grantCallDTO.id)

        mvc.perform(delete("$GRANT_CALLS_URL/${grantCallDTO.id}"))
                .andExpect(status().isNoContent)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test DELETE Grant Calls (NotFound)`() {
        Mockito.`when`((grantCallsServices).deleteGrantCall(grantCallDTO.id)).thenThrow(NotFoundException(""))

        mvc.perform(delete("$GRANT_CALLS_URL/${grantCallDTO.id}"))
                .andExpect(status().isNotFound)
                .andReturn()
    }

    /*getGrantCallApplications*/

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Grant Call Applications (Non EmptyList)`() {
        Mockito.`when`((grantCallsServices).getAllGrantApplications(grantCallDTO.id)).thenReturn(listOf(grantApplicationDAO))

        val result = mvc.perform(get("$GRANT_CALLS_URL/${grantCallDTO.id}/grant_applications"))
                .andExpect(status().isOk)
                .andReturn()

        val response = Utils.toObject<Iterable<GrantApplicationDTO>>(result)
        assertEquals(response, listOf(grantApplicationDTO))
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Grant Call Applications (EmptyList)`() {
        Mockito.`when`((grantCallsServices).getAllGrantApplications(grantCallDTO.id)).thenReturn(listOf())

        val result = mvc.perform(get("$GRANT_CALLS_URL/${grantCallDTO.id}/grant_applications"))
                .andExpect(status().isOk)
                .andReturn()

        val response = Utils.toObject<Iterable<GrantApplicationDTO>>(result)
        assertEquals(response, emptyList)
    }

}
