package fct.ciai.grantsTests.grantCallTest

import fct.ciai.dtos.GrantCallDTO
import fct.ciai.grantsTests.grantCallTest.GrantCallControllerTester.Companion.GRANT_CALLS_URL
import fct.ciai.repositories.grants.GrantCallRepository
import fct.ciai.repositories.sponsor.SponsorRepository
import fct.ciai.services.grants.grantsCalls.GrantCallsServices
import fct.ciai.services.sponsors.SponsorsServices
import fct.ciai.services.users.reviewers.ReviewersServices
import fct.ciai.utils.ObjectsDAO
import fct.ciai.utils.Utils
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class GrantCallAuthorizationTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var sponsorRepository: SponsorRepository

    @MockBean
    lateinit var grantCallRepository: GrantCallRepository

    @MockBean
    lateinit var grantCallsService: GrantCallsServices

    @MockBean
    lateinit var sponsorsServices: SponsorsServices

    @MockBean
    lateinit var reviewersServices: ReviewersServices

    companion object {
        private val sponsor1DAO = ObjectsDAO.sponsor1DAO
        private val sponsor2DAO = ObjectsDAO.sponsor2DAO

        private val grantCallDAO = ObjectsDAO.grantCallDAO

        private val grantCallDTO = GrantCallDTO(grantCallDAO)
    }

    /*Get Grant Call By Id Test*/
    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Grant Call By Id (ADMIN - Authorized)`() {
        Mockito.`when`(grantCallsService.getGrantCallById(grantCallDAO.id)).thenReturn(grantCallDAO)
        mvc.perform(get("$GRANT_CALLS_URL/${grantCallDAO.id}"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test GET Grant Call By Id (STUDENT - Not Authorized)`() {
        Mockito.`when`(grantCallsService.getGrantCallById(grantCallDAO.id)).thenReturn(grantCallDAO)
        mvc.perform(get("$GRANT_CALLS_URL/${grantCallDAO.id}"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test GET Grant Call By Id (REVIEWER - Authorized)`() {
        Mockito.`when`(grantCallsService.getGrantCallById(grantCallDAO.id)).thenReturn(grantCallDAO)
        mvc.perform(get("$GRANT_CALLS_URL/${grantCallDAO.id}"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Grant Call By Id (SPONSOR - Authorized)`() {
        // Need in GrantCallSecurityService call
        Mockito.`when`(sponsorRepository.findByUserName(sponsor1DAO.userName)).thenReturn(Optional.of(sponsor1DAO))
        Mockito.`when`(grantCallRepository.findById(grantCallDAO.id)).thenReturn(Optional.of(grantCallDAO))

        Mockito.`when`(grantCallsService.getGrantCallById(grantCallDAO.id)).thenReturn(grantCallDAO)

        mvc.perform(get("$GRANT_CALLS_URL/${grantCallDAO.id}"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor2", password = "sponsor2", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Grant Call By Id (SPONSOR - Not Authorized)`() {
        // Need in GrantCallSecurityService call
        Mockito.`when`(sponsorRepository.findByUserName(sponsor2DAO.userName)).thenReturn(Optional.of(sponsor2DAO))
        Mockito.`when`(grantCallRepository.findById(grantCallDAO.id)).thenReturn(Optional.of(grantCallDAO))

        mvc.perform(get("$GRANT_CALLS_URL/${grantCallDAO.id}"))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    /*Get ALL GrantCalls Test*/
    @Test
    fun `Test GET All Grant Call (ADMIN - not Authenticated)`() {
        mvc.perform(get("$GRANT_CALLS_URL?status=all"))
                .andExpect(status().isUnauthorized)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Grant Call (ADMIN - Authorized)`() {
        mvc.perform(get("$GRANT_CALLS_URL?status=all"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET All Grant Call (SPONSOR - Authorized)`() {
        mvc.perform(get("$GRANT_CALLS_URL?status=all"))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test GET All Grant Call (STUDENT - Not Authorized)`() {
        mvc.perform(get("$GRANT_CALLS_URL?status=all"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test GET All Grant Call (REVIEWER - Not Authorized)`() {
        mvc.perform(get("$GRANT_CALLS_URL?status=all"))
                .andExpect(status().isOk)
                .andReturn()
    }

    /*CREATE GrantCall Test*/
    @Test
    fun `Test CREATE Grant Call (ADMIN - Not Authenticated)`() {
        mvc.perform(post(GRANT_CALLS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(grantCallDTO)))
                .andExpect(status().isUnauthorized)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Grant Call (ADMIN - Authorized)`() {
        Mockito.`when`(sponsorsServices.getSponsorById(grantCallDAO.sponsor.id)).thenReturn(grantCallDAO.sponsor)
        Mockito.`when`(reviewersServices.getReviewersById(grantCallDAO.evaluationPanel.panelReviewers.map { it.id }))
                .thenReturn(grantCallDAO.evaluationPanel.panelReviewers)
        Mockito.`when`(reviewersServices.getReviewerById(grantCallDAO.evaluationPanel.panelChair.id))
                .thenReturn(grantCallDAO.evaluationPanel.panelChair)

        Mockito.doNothing().`when`(grantCallsService).addGrantCall(grantCallDAO, grantCallDAO.dataItems, grantCallDAO.evaluationPanel)

        mvc.perform(post(GRANT_CALLS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(grantCallDTO)))
                .andExpect(status().isCreated)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test CREATE Grant Call (STUDENT - Not Authorized)`() {
        mvc.perform(post(GRANT_CALLS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(grantCallDTO)))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test CREATE Grant Call (REVIEWER - Not Authorized)`() {
        mvc.perform(post(GRANT_CALLS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(grantCallDTO)))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test CREATE Grant Call (SPONSOR - Authorized)`() {
        //Used by GrantCallSecurity Service class
        Mockito.`when`(sponsorRepository.findByUserName(sponsor1DAO.userName)).thenReturn(Optional.of(sponsor1DAO))
        Mockito.`when`(grantCallRepository.findById(grantCallDAO.id)).thenReturn(Optional.of(grantCallDAO))

        Mockito.`when`(sponsorsServices.getSponsorById(grantCallDAO.sponsor.id)).thenReturn(grantCallDAO.sponsor)
        Mockito.`when`(reviewersServices.getReviewersById(grantCallDAO.evaluationPanel.panelReviewers.map { it.id }))
                .thenReturn(grantCallDAO.evaluationPanel.panelReviewers)
        Mockito.`when`(reviewersServices.getReviewerById(grantCallDAO.evaluationPanel.panelChair.id))
                .thenReturn(grantCallDAO.evaluationPanel.panelChair)

        Mockito.doNothing().`when`(grantCallsService).addGrantCall(grantCallDAO, grantCallDAO.dataItems, grantCallDAO.evaluationPanel)

        mvc.perform(post(GRANT_CALLS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(grantCallDTO)))
                .andExpect(status().isCreated)
                .andReturn()


    }


    /*DELETE Grant Call Test*/
    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test DELETE Grant Call (ADMIN - Authorized)`() {
        Mockito.doNothing().`when`(grantCallsService).deleteGrantCall(grantCallDAO.id)

        mvc.perform(delete("$GRANT_CALLS_URL/${grantCallDAO.id}"))
                .andExpect(status().isNoContent)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test DELETE Grant Call (SPONSOR - Authorized)`() {
        //Used by GrantCallSecurity Service class
        Mockito.`when`(sponsorRepository.findByUserName(sponsor1DAO.userName)).thenReturn(Optional.of(sponsor1DAO))
        Mockito.`when`(grantCallRepository.findById(grantCallDAO.id)).thenReturn(Optional.of(grantCallDAO))

        Mockito.doNothing().`when`(grantCallsService).deleteGrantCall(grantCallDAO.id)
        mvc.perform(delete("$GRANT_CALLS_URL/${grantCallDAO.id}"))
                .andExpect(status().isNoContent)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor2", password = "sponsor2", authorities = ["ROLE_SPONSOR"])
    fun `Test DELETE Grant Call (SPONSOR - Not Authorized)`() {
        //Used by GrantCallSecurity Service class
        Mockito.`when`(sponsorRepository.findByUserName(sponsor2DAO.userName)).thenReturn(Optional.of(sponsor2DAO))
        Mockito.`when`(grantCallRepository.findById(grantCallDAO.id)).thenReturn(Optional.of(grantCallDAO))

        mvc.perform(delete("$GRANT_CALLS_URL/${grantCallDAO.id}"))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test DELETE Grant Call (STUDENT - Not Authorized)`() {
        mvc.perform(delete("$GRANT_CALLS_URL/${grantCallDAO.id}"))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer2", password = "reviewer2", authorities = ["ROLE_REVIEWER"])
    fun `Test DELETE Grant Call (REVIEWER - Not Authorized)`() {
        mvc.perform(delete("$GRANT_CALLS_URL/${grantCallDAO.id}"))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    /*Test Get GrantCall Applications */
    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test Get GrantCall Applications  (ADMIN - Authorized)`() {
        Mockito.`when`(grantCallsService.getAllGrantApplications(grantCallDAO.id)).thenReturn(listOf())
        mvc.perform(get("$GRANT_CALLS_URL/${grantCallDAO.id}/grant_applications"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test Get GrantCall Applications  (REVIEWER - Authorized)`() {
        Mockito.`when`(grantCallsService.getAllGrantApplications(grantCallDAO.id)).thenReturn(listOf())
        mvc.perform(get("$GRANT_CALLS_URL/${grantCallDAO.id}/grant_applications"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test Get GrantCall Applications (SPONSOR - Authorized)`() {
        //Used by GrantCallSecurity Service class
        Mockito.`when`(sponsorRepository.findByUserName(sponsor1DAO.userName)).thenReturn(Optional.of(sponsor1DAO))
        Mockito.`when`(grantCallRepository.findById(grantCallDAO.id)).thenReturn(Optional.of(grantCallDAO))

        Mockito.`when`(grantCallsService.getAllGrantApplications(grantCallDAO.id)).thenReturn(listOf())

        mvc.perform(get("$GRANT_CALLS_URL/${grantCallDAO.id}/grant_applications"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor2", password = "sponsor2", authorities = ["ROLE_SPONSOR"])
    fun `Test Get GrantCall Applications (SPONSOR - Not Authorized)`() {
        //Used by GrantCallSecurity Service class
        Mockito.`when`(sponsorRepository.findByUserName(sponsor2DAO.userName)).thenReturn(Optional.of(sponsor2DAO))
        Mockito.`when`(grantCallRepository.findById(grantCallDAO.id)).thenReturn(Optional.of(grantCallDAO))

        mvc.perform(get("$GRANT_CALLS_URL/${grantCallDAO.id}/grant_applications"))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test Get GrantCall Applications  (STUDENT - Not Authorized)`() {
        mvc.perform(get("$GRANT_CALLS_URL/${grantCallDAO.id}/grant_applications"))
                .andExpect(status().isForbidden)
                .andReturn()
    }


}
