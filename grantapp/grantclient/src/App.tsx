import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';

import HomePage from "./app_components/HomePage";

import StudentHomePage from "./app_components/student/StudentHomePage";
import GrantApplicationDetails from "./app_components/GrantApplicationDetails";
import GrantApplicationsList from "./app_components/GrantApplicationsList";
import LoginPage from "./app_components/LoginPage";


import AllGrantCalls from "./app_components/anonymous/AllGrantCalls";
import ReviewerHomePage from "./app_components/reviewer/ReviewerHomePage";
import GrantCallsDetails from "./app_components/grants/GrantCallDetails";
import StudentGrantCallsList from "./app_components/student/StudentGrantCallsList";
import GrantCallForm from "./app_components/grants/GrantCallForm";
import OpenGrantCallsListAnonymous from "./app_components/anonymous/OpenGrantCallsListAnonymous";
import GrantCallDetailsAnonymous from "./app_components/anonymous/GrantCallDetailsAnonymous";
import CloseGrantCalls from "./app_components/anonymous/CloseGrantCalls";
import FundedGrants from "./app_components/anonymous/FundedGrants";
import GrantCallsToReview from "./app_components/reviewer/GrantCallsToReview";
import GrantCallApplicationsList from "./app_components/reviewer/GrantCallApplications";
import ReviewerGrantApplicationDetails from "./app_components/reviewer/ReviewerGrantApplicationDetails";
import StudentDetails from "./app_components/reviewer/StudentDetails";
import ChairedGrantCalls from "./app_components/reviewer/ChairedGrantCalls";
import GrantApplicationReviews from "./app_components/reviewer/GrantApplicationReviews";
import ReviewCreationForm from "./app_components/reviewer/ReviewCreationForm";


function App() {

    return (
        <Router>
            <Switch>
                <Route path="/home" exact component={HomePage}/>
                <Route path="/home/login" exact component={LoginPage}/>
                <Route path="/home/grant_calls" exact component={OpenGrantCallsListAnonymous}/>
                <Route path="/home/grant_calls/all" exact component={AllGrantCalls}/>
                <Route path="/home/grant_calls/:id" exact component={GrantCallDetailsAnonymous}/>
                <Route path="/home/close_grant_calls" exact component={CloseGrantCalls}/>
                <Route path="/home/close_grant_calls/:id" exact component={FundedGrants}/>

                <Route path="/home/student" exact component={StudentHomePage}/>
                <Route path="/student/grant_calls" exact component={StudentGrantCallsList}/>
                <Route path="/student/grant_applications" exact component={GrantApplicationsList}/>
                <Route path="/student/grant_application/:id" component={GrantApplicationDetails}/>
                <Route path="/student/grant_calls/:id/form" component={GrantCallForm}/>
                <Route path="/student/grant_calls/:id" component={GrantCallsDetails}/>

                <Route path="/home/reviewer" component={ReviewerHomePage}/>
                <Route path="/reviewer/grants_for_review" component={GrantCallsToReview}/>
                <Route path="/reviewer/grant_calls/:id/grant_applications/" component={GrantCallApplicationsList}/>
                <Route path="/reviewer/grant_application/:id" component={ReviewerGrantApplicationDetails}/>
                <Route path="/student/:id" component={StudentDetails}/>
                <Route path="/reviewer/chaired_grants" component={ChairedGrantCalls}/>
                <Route path="/grant_application/:id/reviews/creation_form" component={ReviewCreationForm}/>
                <Route path="/grant_application/:id/reviews" component={GrantApplicationReviews}/>

                <Route path="/">
                    <Redirect to="/home"/>
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
