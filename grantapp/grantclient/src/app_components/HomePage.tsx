import React from "react";
import {Link} from 'react-router-dom';
import {FcGraduationCap, FcLock, FcMindMap} from "react-icons/fc";
import fctLogo from "../images/fct_logo.png";
import {GrantCallStatus} from "./anonymous/AllGrantCalls";

const HomePage = () => {
    return (
        <div className="bg-gradient-primary m-5">
            <div className="rounded p-3">
                <img className="fct" src={fctLogo} alt="fctLogo"/>
                <button className="btn btn-primary float-right ml-3 mr-5">Sign in</button>
                <Link to="/home/login" className="btn btn-secondary float-right">Login</Link>
            </div>
            <div className="image-group">
                <div className="image-block btn-dark m-4 rounded shadow w-25">
                    <Link to="/home/grant_calls" style={{ textDecoration: 'none' }}>
                        <FcGraduationCap size={100}/>
                        <h3 className="text-light">Open Grant Calls</h3>
                    </Link>
                </div>
                <div className="image-block btn-dark m-4 rounded shadow w-25">
                    <Link to={{pathname: "/home/grant_calls/all", state: {status: GrantCallStatus.ALL}}} style={{ textDecoration: 'none' }}>
                        <FcMindMap size={100}/>
                        <h3 className="text-light">All Grant Calls</h3>
                    </Link>
                </div>
                <div className="image-block btn-dark m-4 rounded shadow w-25">
                    <Link to={{pathname: "/home/close_grant_calls", state: {status: GrantCallStatus.CLOSED}}} style={{ textDecoration: 'none' }}>
                        <FcLock size={100}/>
                        <h3 className="text-light">Closed Grant Calls</h3>
                    </Link>
                </div>
            </div>
        </div>
    );
}

export default HomePage;
