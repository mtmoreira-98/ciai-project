import React from "react";
import {Redirect} from 'react-router-dom';
import {DataItemDTO, LoginControllerApiFp} from "../typescript-fetch-client";
import {GoBackButton} from "./GrantApplicationsList";
import {Card} from "react-bootstrap";


export type AccountInfo = {
    username: string,
    password: string,
    accountId: number,
    name: string,
}


type LoginPageState = {
    username: string,
    password: string,
    accountId: number,
    roles: string[],
    fullName: string,
    loggedIn: boolean,
    loggedFailed: boolean

}

class LoginPage extends React.Component<{}, LoginPageState> {

    constructor(props: {}) {
        super(props);
        this.state = {
            username: '',
            password: '',
            accountId: undefined,
            fullName: '',
            roles: [],
            loggedIn: false,
            loggedFailed: false
        };
    }

    handleUsernameChange = (event: any) => {
        this.setState(() => ({username: event.target.value}))
    }

    handlePasswordChange = (event: any) => {
        this.setState(() => ({password: event.target.value}))
    }

    loginRequest = async () => {
        const form = {
            username: this.state.username,
            password: this.state.password,
        }
        await LoginControllerApiFp().loginUsingPOST(form)().then(result => {
            this.setState(() => ({
                loggedIn: true,
                accountId: result.accountDTO.id,
                roles: result.accountRoles,
                fullName: result.accountDTO.name
            }))
        }).catch(() => {
                this.setState(() => ({
                    loggedFailed: true
                }))
            }
        )


    }

    render() {

        if (this.state.loggedIn) {
            switch (this.state.roles[0]) {
                case "ROLE_STUDENT": {
                    return (<Redirect to={{
                        pathname: "/home/student",
                        state: {
                            username: this.state.username,
                            password: this.state.password,
                            accountId: this.state.accountId,
                            name: this.state.fullName,
                        }
                    }}/>)
                }
                case "ROLE_REVIEWER": {
                    return (<Redirect to={{
                        pathname: "/home/reviewer",
                        state: {
                            username: this.state.username,
                            password: this.state.password,
                            accountId: this.state.accountId,
                            name: this.state.fullName,
                        }
                    }}/>)
                }
                default: {
                    throw ("No match for role " + this.state.roles[0]);
                }
            }
        } else {
            return (
                <div className="modal-content modal-dialog" role="document">
                    <div className="bg-dark card-heading text-center">
                        <h4 className=" modal-title w-100 font-weight-bold">Login</h4>
                    </div>
                    <div className="modal-body mx-3">
                        <div className="md-form m-4">
                            <i className="fas fa-envelope prefix grey-text"/>

                            <input value={this.state.username} onChange={this.handleUsernameChange} type="email"
                                   id="defaultForm-email" className="form-control validate" placeholder="Account Name"/>
                        </div>

                        <div className="md-form m-4">
                            <i className="fas fa-lock prefix grey-text"/>
                            <input value={this.state.password} onChange={this.handlePasswordChange}
                                   type="password" id="defaultForm-pass" className="form-control validate" placeholder="Password"/>
                        </div>
                        <button onClick={this.loginRequest} className="btn btn-primary float-right login-form-button">Login
                        </button>
                        <GoBackButton/>
                        <LoginError hasFailed={this.state.loggedFailed}/>

                    </div>
                </div>
            )
        }
    }
}

function LoginError(props) {
    if (props.hasFailed) {
        return (
            <div style={{color: 'red'}}> Please insert a valid username and password.</div>
        )
    }
    return (<div></div>)
}

export default LoginPage;
