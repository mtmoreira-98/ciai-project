import React, {useEffect, useState} from "react";
import axios from 'axios';
import '../../styles/global_styles.css'


type ListState = { students: StudentDTO[] }

interface StudentDTO {
    'id'?: number;
    'userName': string;
    'name': string;
    'password': string;
    'email': string;
    'birthDate': Date;
    'institution'?: number;
    'average'?: number;
    'cv'?: number
}

class StudentRow extends React.Component<ListState, {}> {
    render() {
        return (
            <table className="table">
                <tbody>
                <tr>
                    <th>Id</th>
                    <th>Username</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>BirthDate</th>
                    <th>Institution</th>
                    <th>Average</th>
                    <th>CV Id</th>
                </tr>

                {this.props.students.map((student: StudentDTO) => (
                    <tr>
                        <td>{student.id} </td>
                        <td>{student.userName} </td>
                        <td>{student.name} </td>
                        <td>{student.email} </td>
                        <td>{student.birthDate}</td>
                        <td>{student.institution} </td>
                        <td>{student.average} </td>
                        <td>{student.cv} </td>
                    </tr>

                ))}
                </tbody>
            </table>
        );
    }
}

/*const StudentRow = (students: StudentDTO[]) => {
    return (
        <table>
            <tr>
                <th>Id</th>
                <th>Username</th>
                <th>Name</th>
                <th>Password</th>
                <th>Email</th>
                <th>BirthDate</th>
                <th>Institution</th>
                <th>Average</th>
                <th>CV Id</th>
            </tr>

            {students.map((student: StudentDTO) => (
                <tr>
                    <td>{student.id} </td>
                    <td>{student.userName} </td>
                    <td>{student.name} </td>
                    <td>{student.password} </td>
                    <td>{student.email} </td>
                    <td>{student.birthDate} </td>
                    <td>{student.institution} </td>
                    <td>{student.average} </td>
                    <td>{student.cv} </td>
                </tr>

            ))}
        </table>
    )
}
*/
function StudentsList(status: any) {

    const [listState, setList] = useState<ListState>({students: []})

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(/*'/grant_calls?'+status*/'/students', {
                auth: {
                    username: 'admin',
                    password: 'admin',
                },
            })
            setList({students: [...result.data]})
        }

        fetchData();
    }, []);

    return (
        <div>
            <ul>
                <StudentRow students={listState.students}/>
            </ul>
        </div>
    );
}

export default StudentsList;
