import React from "react";
import {Link} from 'react-router-dom';

import {GrantApplicationStatusEnum} from "../../typescript-fetch-client";
import {AccountInfo} from "../LoginPage";
import {RouteComponentProps} from 'react-router';
import {GrantCallStatus} from "../anonymous/AllGrantCalls";
import { FcGraduationCap, FcSurvey, FcUpload, FcApproval, FcHighPriority} from "react-icons/fc";
import Breadcrumb from 'react-bootstrap/Breadcrumb'


class StudentHomePage extends React.Component<RouteComponentProps<AccountInfo>, AccountInfo> {

    constructor(props) {
        super(props);
        this.state = this.props.location.state
    }

    render() {
        return (
            <div className="m-5">
                <div className="header bg-dark rounded text-light p-4">
                    Student: {this.state.name}
                    <Link to="/home" className="btn btn-secondary float-right">Logout</Link>
                </div>

                <div className="image-group">

                    <Link className="" to={{
                        pathname: "/student/grant_calls",
                        state: {status: GrantCallStatus.OPEN, student: this.state}
                    }}>
                        <div className="image-block btn-dark m-4 rounded shadow w-25">

                            <FcGraduationCap size={100}/>
                            <h3 className="text-light">Open Grant Calls</h3>

                        </div>
                    </Link>


                    <Link to={{
                        pathname: "/student/grant_applications",
                        state: {status: GrantApplicationStatusEnum.CREATED, student: this.state}
                    }}>
                        <div className="image-block btn-dark m-4 rounded shadow w-25">
                            <FcSurvey size={100}/>
                            <h3 className="text-light">Created Grant Applications</h3>
                        </div>
                    </Link>

                    <Link to={{
                        pathname: "/student/grant_applications",
                        state: {status: GrantApplicationStatusEnum.SUBMITTED, student: this.state}
                    }}>
                        <div className="image-block btn-dark m-4 rounded shadow w-25">

                            <FcUpload size={100}/>
                            <h3 className="text-light">Submitted Grant Applications</h3>
                        </div>
                    </Link>

                    <Link to={{
                        pathname: "/student/grant_applications",
                        state: {status: GrantApplicationStatusEnum.ACCEPTED, student: this.state}
                    }}>
                        <div className="image-block btn-dark m-4 rounded shadow w-25">

                            <FcApproval size={100}/>
                            <h3 className="text-light">Accepted Grant Applications</h3>
                        </div>
                    </Link>

                    <Link className="" to={{
                        pathname: "/student/grant_applications",
                        state: {status: GrantApplicationStatusEnum.REJECTED, student: this.state}
                    }}>
                        <div className="image-block btn-dark m-4 rounded shadow w-25">
                            <FcHighPriority size={100}/>
                            <h3 className="text-light">Rejected Grant Applications</h3>
                        </div>
                    </Link>

                </div>
            </div>
        );
    }
}

export default StudentHomePage;
