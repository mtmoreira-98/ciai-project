import React, {Component} from "react";
import '../../styles/global_styles.css'
import {GrantCallsControllerApiFp, GrantCallSponsorDTO} from "../../typescript-fetch-client";
import {GoBackButton} from "../GrantApplicationsList";
import {RouteComponentProps} from 'react-router';
import {dateFormat} from "../../utils/Utils";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import {Link} from "react-router-dom";
import Loader from 'react-loader-spinner';


type ListRows = {
    grantCalls: GrantCallSponsorDTO[]
}

class GrantCallRow extends Component<ListRows, {}> {

    render() {
        return (
            <table className="table ">
                <thead className="thead-dark">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Title</th>
                    <th scope="col">Start Date</th>
                    <th scope="col">End Date</th>
                    <th scope="col">#Applications</th>
                </tr>

                </thead>
                <tbody>
                {this.props.grantCalls.map((grantCall: GrantCallSponsorDTO) => (

                    <tr key={grantCall.id}>
                        <th scope="row">
                            <Link className="no_style" to={"/home/grant_calls/" + grantCall.id}>
                                <h6>{grantCall.id}</h6>
                            </Link>
                        </th>
                        <td>
                            <Link className="no_style" to={"/home/grant_calls/" + grantCall.id}>
                                {grantCall.title}
                            </Link>
                        </td>
                        <td>
                            {dateFormat(grantCall.startDate)}
                        </td>
                        <td>
                            {dateFormat(grantCall.expireDate)}
                        </td>
                        <td>
                            {grantCall.numberOfApplications}
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
        );
    }
}

interface GrantCallsListProps {
}

type GrantCallsListState = {
    grantCalls: GrantCallSponsorDTO[]
    isFetching: boolean
}


class OpenGrantCallsListAnonymous extends Component<RouteComponentProps<GrantCallsListProps>, GrantCallsListState> {
    constructor(props: GrantCallsListProps) {
        super(props);
        this.state = {
            grantCalls: [],
            isFetching: true
        };
    }

    async componentDidMount() {
        await GrantCallsControllerApiFp().getGrantCallsOnStatusCompleteUsingGET("OPEN", {
            headers: {
                'Authorization': 'Basic ' + btoa("admin" + ':' + "admin"),
            }
        })().then(result => this.setState(() => ({
            grantCalls: result,
            isFetching: false
        })));
    }

    render() {
        if (this.state.isFetching)
            return (<Loader className="center" type="TailSpin" color="#CCC" height={60} width={60} timeout={3000}/>)

        return (
            <div className="m-5">
                <h2>Open Grant Calls</h2>
                <Breadcrumb>
                    <Breadcrumb.Item linkAs={Link} linkProps={{to:{pathname:"/home"}}}>Home</Breadcrumb.Item>
                    <Breadcrumb.Item active>Open grant calls</Breadcrumb.Item>
                </Breadcrumb>
                <GrantCallRow grantCalls={this.state.grantCalls}/>
                <GoBackButton/>
            </div>
        );
    }
}

export default OpenGrantCallsListAnonymous;
