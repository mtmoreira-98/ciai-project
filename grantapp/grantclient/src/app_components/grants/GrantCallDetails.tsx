import {GrantCallsControllerApiFp, GrantCallSponsorDTO} from "../../typescript-fetch-client";
import React from "react";
import {AccountInfo} from "../LoginPage";
import {RouteComponentProps} from 'react-router';
import Loader from 'react-loader-spinner';
import {GoBackButton} from "../GrantApplicationsList";
import {Link} from "react-router-dom";
import {dateFormat} from "../../utils/Utils";
import StatusComponent from "../components/StatusComponent";

interface GrantCallsProps {
    grantCallId: number,
    account: AccountInfo
}

type GrantCallListState = {
    grantCallId: number,
    account: AccountInfo
    grantCall: GrantCallSponsorDTO
    isFetching: boolean
}

class GrantCallsDetails extends React.Component<RouteComponentProps<GrantCallsProps>, GrantCallListState> {

    constructor(props: GrantCallsProps) {
        super(props);
        this.state = {
            grantCallId: this.props.match.params.id,
            account: this.props.location.state.account,
            grantCall: undefined,
            isFetching: true,
        };
    }

    async componentDidMount() {

        await GrantCallsControllerApiFp().getGrantCallByIdCompleteUsingGET(this.state.grantCallId, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.account.username + ':' + this.state.account.password),
            }
        })().then((result: GrantCallSponsorDTO) => this.setState(() => ({
            grantCall: result,
            isFetching: false
        })));
    }

    render() {
        if (this.state.isFetching)
            return (
                <Loader className="center"
                        type="TailSpin"
                        color="#CCC"
                        height={60}
                        width={60}
                        timeout={3000}
                />)
        else {
            return (
                <div>
                    <div className="m-2">
                        <div className="header">
                            Grant Call Details
                        </div>
                        <div className="top-right mt-2">
                            <StatusComponent status={this.state.grantCall.status.toString()}/>
                        </div>
                    </div>
                    <hr className="hr-style"/>
                    <div className="m-5">
                        <div className="info-title">Sponsor:</div>
                        <div className="info-data">{this.state.grantCall.sponsorName}</div>
                        <div className="info-title">Description:</div>
                        <div className="info-data">{this.state.grantCall.description}</div>
                        <div className="info-title">Start Date:</div>
                        <div className="info-data">{dateFormat(this.state.grantCall.startDate)}</div>
                        <div className="info-title">End Date:</div>
                        <div className="info-data">{dateFormat(this.state.grantCall.expireDate)}</div>
                        <div className="info-title">Funding:</div>
                        <div className="info-data">{this.state.grantCall.funding}€</div>
                    </div>
                    <div className="m-5 mb-10">
                        <Link className="no_style" to={{
                            pathname: '/student/grant_calls/' + this.state.grantCall.id + '/form',
                            state: {account: this.state.account}
                        }}>
                            <button className="btn btn-primary float-right ml-3">Apply to Grant</button>
                        </Link>

                        <div>
                            <GoBackButton/>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default GrantCallsDetails;
