import {
    DataItemDTO, DataItemResponseDTO, GrantApplicationDTO, GrantApplicationsControllerApiFp,
    GrantCallsControllerApiFp,
    GrantCallSponsorDTO,
    GrantApplicationStatusEnum
} from "../../typescript-fetch-client";
import React from "react";
import {AccountInfo} from "../LoginPage";
import {RouteComponentProps} from 'react-router';
import Loader from 'react-loader-spinner';
import {GoBackButton} from "../GrantApplicationsList";
import '../../styles/global_styles.css';
import {Card} from "react-bootstrap";
import {Redirect} from "react-router-dom";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

interface GrantCallsProps {
    grantCallId: number,
    account: AccountInfo
}

type GrantCallListState = {
    grantCallId: number,
    account: AccountInfo
    grantCall: GrantCallSponsorDTO
    isFetching: boolean
    submission: GrantApplicationDTO
    dataItemResponses: Map<string, DataItemResponseDTO>,
    submissionAccepted: boolean | undefined
}

class GrantCallForm extends React.Component<RouteComponentProps<GrantCallsProps>, GrantCallListState> {

    constructor(props: GrantCallsProps) {
        super(props);
        this.state = {
            grantCallId: this.props.match.params.id,
            account: this.props.location.state.account,
            grantCall: undefined,
            isFetching: true,
            submission: undefined,
            dataItemResponses: new Map(),
            submissionAccepted: undefined,
        };
    }


    async componentDidMount() {
        await GrantCallsControllerApiFp().getGrantCallByIdCompleteUsingGET(this.state.grantCallId, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.account.username + ':' + this.state.account.password),
            }
        })().then((result: GrantCallSponsorDTO) => {
            const dataItemResponse = result.dataItems.map((dataItem: DataItemDTO) => ({
                dataItemId: dataItem.id,
                grantApplicationId: 0,
                id: 0,
                value: undefined
            } as DataItemResponseDTO));

            result.dataItems.map((dataItem: DataItemDTO) => ({
                dataItemId: dataItem.id,
                grantApplicationId: 0,
                id: 0,
                value: undefined
            } as DataItemResponseDTO));

            this.setState(() => ({
                grantCall: result,
                submission: {
                    fieldsResponses: dataItemResponse,
                    grantCallId: this.state.grantCallId,
                    id: 0,
                    status: GrantApplicationStatusEnum.CREATED,
                    studentID: this.state.account.accountId
                } as GrantApplicationDTO,
                isFetching: false,
            }));
        }).catch(() => {
        });
    }

    updateComponent = (event: any) => {
        const dataItemResponse = ({
            dataItemId: Number(event.target.name),
            grantApplicationId: 0, // does not matter
            id: 0, // does not matter
            value: event.target.value
        } as DataItemResponseDTO);


        this.setState((oldState) => (
            {dataItemResponses: oldState.dataItemResponses.set(event.target.name, dataItemResponse)})
        );
    }

    submitForm = async (event: any) => {
        await this.setState(() => ({
            submission: {
                fieldsResponses: Array.from<DataItemResponseDTO>(this.state.dataItemResponses.values()),
                grantCallId: this.state.grantCallId,
                id: 0,
                status: GrantApplicationStatusEnum.CREATED,
                studentID: this.state.account.accountId
            } as GrantApplicationDTO,
        }));


        event.preventDefault();
        await GrantApplicationsControllerApiFp().createGrantApplicationUsingPOST(this.state.submission, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.account.username + ':' + this.state.account.password),
            }
        })().then(() => {
            confirmAlert({
                title: 'Grant Application created',
                message: 'Your application was successfully created',
                buttons: [
                    {
                        label: 'Ok',
                        onClick: () => this.setState({submissionAccepted: true})
                    },
                ]
            });
        })
            .catch(() => {
                this.setState(() => ({
                    submissionAccepted: false
                }))
            });


    }

    render() {
        if (this.state.isFetching)
            return (
                <Loader className="center"
                        type="TailSpin"
                        color="#CCC"
                        height={60}
                        width={60}
                        timeout={3000}
                />)
        else {
            return (
                <Card bsPrefix="card-card-5">
                    <div className="card-heading">
                        <h2 className="title">{this.state.grantCall.title} Application Form</h2>
                    </div>
                    <div className="card-body">
                        <div className="">
                            <form onSubmit={this.submitForm}>
                                {this.state.grantCall.dataItems.map((currDataItem: DataItemDTO) => (
                                    <div className="form-group required" key={currDataItem.id}>
                                        <label className="d-flex flex-row">{currDataItem.header}
                                            {currDataItem.mandatory ? <div className="text-danger"> *</div> : <></>}
                                        </label>
                                        <input type={currDataItem.type} className="form-control"
                                               aria-describedby="emailHelp"
                                               placeholder={"Enter " + currDataItem.header}
                                               onChange={this.updateComponent} name={currDataItem.id.toString()}/>
                                        <small id="emailHelp"
                                               className="form-text text-muted">{currDataItem.subTitle}</small>
                                    </div>
                                ))}
                                <ErrorComponent submissionAccepted={this.state.submissionAccepted}
                                                session={this.state.account}/>
                                <input type="submit" className="btn btn-primary" value="Submit"/>
                                <GoBackButton/>
                            </form>
                        </div>
                    </div>
                </Card>
            );
        }
    }
}


class ErrorComponent extends React.Component<{ submissionAccepted: boolean | undefined, session: AccountInfo }, {}> {

    render() {
        if (this.props.submissionAccepted === undefined)
            return (<></>)

        if (!this.props.submissionAccepted)
            return (<div className="text-danger mb-3"> Please insert all the mandatory fields. (*)</div>)

        return (
            <Redirect to={{
                pathname: "/home/student",
                state: this.props.session
            }}/>
        )
    }

}

export default GrantCallForm;
