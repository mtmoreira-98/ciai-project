import React from "react";
import '../../styles/global_styles.css'
import Loader from 'react-loader-spinner'
import {GoBackButton} from "../GrantApplicationsList";
import {
    DataItemResponseDetailsDTO,
    GrantApplicationDetailsDTO,
    GrantApplicationsControllerApiFp, ReviewersControllerApiFp, ReviewsControllerApiFp
} from "../../typescript-fetch-client";
import {RouteComponentProps} from 'react-router';
import {AccountInfo} from "../LoginPage";
import {Link} from "react-router-dom";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import StatusComponent from "../components/StatusComponent";

type GrantAppState = {
    reviewer: AccountInfo,
    grantApplication: GrantApplicationDetailsDTO,
    isPanelChair: boolean,
    alreadyHasReview: boolean,
}

class ReviewerGrantApplicationDetails extends React.Component<RouteComponentProps<AccountInfo>, GrantAppState> {
    constructor(props) {
        super(props);
        this.state = {
            reviewer: this.props.location.state.reviewer,
            grantApplication: undefined,
            isPanelChair: undefined,
            alreadyHasReview: undefined,
        }
    }

    async componentDidMount() {
        const grantApplicationId = this.props.match.params.id
        await GrantApplicationsControllerApiFp().getGrantApplicationDetailsByIdUsingGET(grantApplicationId, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.reviewer.username + ':' + this.state.reviewer.password),
            }
        })().then(result => this.setState(() => ({
                grantApplication: result,
            }))
        )
        await this.checkIfHasReview()
    }

    async checkIfHasReview() {
        ReviewsControllerApiFp().checkIfAlreadyReviewedUsingGET(this.state.grantApplication.id, this.state.reviewer.accountId, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.reviewer.username + ':' + this.state.reviewer.password),
            }
        })().then(() => this.setState(() => ({
                alreadyHasReview: true,
            }))
        ).catch(async () => {
            this.setState(() => ({
                alreadyHasReview: false,
               //isPanelChair: false,
            }))
            await this.checkIfPanelChair()
        })

    }

    async checkIfPanelChair() {
        ReviewersControllerApiFp().checkIfReviewerIsChairUsingGET(this.state.grantApplication.grantCallId, this.state.reviewer.accountId, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.reviewer.username + ':' + this.state.reviewer.password),
            }
        })().then(() => this.setState(() => ({
                isPanelChair: true,
            }))
        ).catch(() => this.setState(() => ({
            isPanelChair: false,
        })))
    }

    render() {
        if (this.state.grantApplication === undefined || this.state.alreadyHasReview === undefined
            || (this.state.alreadyHasReview === false && this.state.isPanelChair === undefined)) {
            return (
                <Loader className="center" type="TailSpin" color="#CCC" height={60} width={60} timeout={3000}/>
            )
        } else {
            return (

                <div>
                    <div>
                        <div className="header">
                            Grant Application Details
                        </div>
                        <div className="top-right">
                            <StatusComponent status={this.state.grantApplication.status.toString()}/>
                        </div>
                        <div className="m-3">
                        <Breadcrumb className = "breadcrumb-white-bg">
                            <Breadcrumb.Item linkAs={Link} linkProps={{to:{pathname:"/home/reviewer", state: this.state.reviewer}}}>Home</Breadcrumb.Item>
                            <Breadcrumb.Item linkAs={Link} linkProps={{to:{pathname:"/reviewer/grants_for_review", state: {reviewer: this.state.reviewer}}}}>Grant Calls</Breadcrumb.Item>
                            <Breadcrumb.Item linkAs={Link} linkProps={{to:{pathname:`/reviewer/grant_calls/${this.state.grantApplication.grantCallId}/grant_applications/`, state: {reviewer: this.state.reviewer}}}}>Grant Call {this.state.grantApplication.grantCallId}</Breadcrumb.Item>
                            <Breadcrumb.Item active>Grant Application {this.state.grantApplication.id}</Breadcrumb.Item>
                        </Breadcrumb>
                        </div>
                    </div>


                    {this.state.alreadyHasReview ?
                        <div/>
                        :
                        <Link className="no_style" to={{
                            pathname: '/grant_application/' + this.state.grantApplication.id + '/reviews/creation_form',
                            state: {
                                account: this.state.reviewer,
                                isChair: this.state.isPanelChair,
                                callName: this.state.grantApplication.grantCallName
                            }
                        }}>
                            <button type="button" className="btn btn-success float-right mr-5 ml-3 mt-3  pb-2 pt-2">
                                {this.state.isPanelChair ? "Add Final Review" : "Add Review"}
                            </button>
                        </Link>
                    }

                    <div>
                        <div className="info-title"> Grant Call:</div>
                        <div className="info-data">{this.state.grantApplication.grantCallName}</div>
                        <div className="info-title"> Student:</div>
                        <div className="info-data">{this.state.grantApplication.studentName}</div>
                    </div>
                    <div>
                        {this.state.grantApplication.fieldsResponses.map((fieldResponse: DataItemResponseDetailsDTO) => (
                            <div key={fieldResponse.id}>
                                <div className="info-title">
                                    {fieldResponse.title}:
                                </div>
                                <div className="info-data">
                                    {fieldResponse.value}
                                </div>
                            </div>
                        ))}
                    </div>
                    <div className = "m-3">
                    <Link className="no_style" to={{
                        pathname: '/student/' + this.state.grantApplication.studentID,
                        state: {account: this.state.reviewer}
                    }}>
                        <button type="button" className="btn btn-primary float-right mr-5 ml-3">
                            Student Details
                        </button>
                    </Link>
                    <Link className="no_style" to={{
                        pathname: '/grant_application/' + this.state.grantApplication.id + '/reviews',
                        state: {account: this.state.reviewer , callName: this.state.grantApplication.grantCallName}
                    }}>
                        <button type="button" className="btn btn-primary float-right ml-3">
                            See Reviews
                        </button>
                    </Link>
                    <GoBackButton/>
                    </div>
                </div>
            )
        }
    };


}


export default ReviewerGrantApplicationDetails;
