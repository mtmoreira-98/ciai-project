import React from "react";
import '../../styles/global_styles.css'
import Loader from 'react-loader-spinner'
import {GoBackButton} from "../GrantApplicationsList";
import {FinalReviewEnum, GrantApplicationsControllerApiFp, ReviewDetailsDTO,} from "../../typescript-fetch-client";
import {RouteComponentProps} from 'react-router';
import {AccountInfo} from "../LoginPage";


class Review extends React.Component<{ review: ReviewDetailsDTO }, {}> {
    render() {
        switch (this.props.review.finalReview) {
            case FinalReviewEnum.NORMAL:
                return (
                    <div className="normal-bg m-5 pl-3 pt-4 pb-4">
                        <h5 className="pl-4 pb-3">Normal Review</h5>
                        <div className="info-title">Grade:</div>
                        <div className="info-data">{this.props.review.note}</div>
                        <div className="info-title"> Evaluation:</div>
                        <div className="info-data">{this.props.review.description}</div>
                        <div className="info-title"> Reviewer:</div>
                        <div className="info-data">{this.props.review.reviewer.name}</div>

                    </div>
                )
            case FinalReviewEnum.FINALACCEPTED:
                return (
                    <div className="accepted-bg m-5 pl-3 pt-4 pb-4">
                        <h5 className="pl-4 pb-3">Final Review</h5>
                        <div className="info-title">Grade:</div>
                        <div className="info-data">{this.props.review.note}</div>
                        <div className="info-title"> Evaluation:</div>
                        <div className="info-data">{this.props.review.description}</div>
                        <div className="info-title"> Reviewer:</div>
                        <div className="info-data">{this.props.review.reviewer.name}</div>
                    </div>
                )
            case FinalReviewEnum.FINALREJECTED:
                return (
                    <div className="rejected-bg m-5 pl-3 pt-4 pb-4">
                        <h5 className="pl-4 pb-3">Final Review</h5>
                        <div className="info-title">Grade:</div>
                        <div className="info-data">{this.props.review.note}</div>
                        <div className="info-title"> Evaluation:</div>
                        <div className="info-data">{this.props.review.description}</div>
                        <div className="info-title"> Reviewer:</div>
                        <div className="info-data">{this.props.review.reviewer.name}</div>
                    </div>
                )
        }
    }
}

type GrantAppState = {
    reviewer: AccountInfo,
    grantAppId: number,
    isFetching: boolean
    reviews: ReviewDetailsDTO[]
    grantCallName: string
}

class GrantApplicationReviews extends React.Component<RouteComponentProps<AccountInfo>, GrantAppState> {
    constructor(props) {
        super(props);
        this.state = {
            reviewer: this.props.location.state.account,
            grantAppId: this.props.match.params.id,
            isFetching: true,
            reviews: undefined,
            grantCallName: this.props.location.state.callName,

        }
    }

    componentDidMount() {
        GrantApplicationsControllerApiFp().getReviewsWithAuthorByGrantApplicationIdUsingGET(this.state.grantAppId, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.reviewer.username + ':' + this.state.reviewer.password),
            }
        })().then(result => this.setState(() => ({
                reviews: result,
                isFetching: false,
            }))
        )
    }

    render() {

        if (this.state.isFetching) {
            return (
                <Loader className="center" type="TailSpin" color="#CCC" height={60} width={60} timeout={3000}/>
            )
        } else if (this.state.reviews.length == 0) {
            return (<div className="m-5">
                    <div className="header">
                        Reviews
                    </div>
                    <hr className="hr-style"/>
                    <div>
                        <h4 className="text-muted d-flex justify-content-center p-5">Nothing to Show ...</h4>
                    </div>

                    <GoBackButton/>

                </div>
            )

        } else {
            return (
                <div>
                    <div>
                        <div className="header">
                            Reviews
                        </div>
                        <div className="top-right">
                            {this.state.grantCallName}
                        </div>
                    </div>

                    <hr className="hr-style"/>
                    <div className="p-2 pr-5">
                        <GoBackButton/>
                    </div>
                    <div>
                        {this.state.reviews.reverse().map((review: ReviewDetailsDTO) => (
                            <div key={review.id}>
                                <Review review={review}/>
                            </div>
                        ))}
                    </div>

                </div>
            )
        }
    };


}


export default GrantApplicationReviews;
