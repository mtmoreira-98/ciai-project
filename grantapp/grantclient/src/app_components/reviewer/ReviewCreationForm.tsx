import React from "react";
import {Redirect} from 'react-router-dom';
import {GoBackButton} from "../GrantApplicationsList";
import {RouteComponentProps} from 'react-router';
import {FinalReviewDTO, ReviewDTO, ReviewsControllerApiFp} from "../../typescript-fetch-client";
import {AccountInfo} from "../LoginPage";
import {confirmAlert} from "react-confirm-alert";


export type Props = {
    account: AccountInfo,
    isChair: boolean
    callName: string
}


type LoginPageState = {
    grantApplicationId: number,
    isPanelChair: boolean,
    reviewer: AccountInfo,
    grade: number,
    content: string,
    submissionError: string,
    submissionOk: boolean,
    acceptGrant: boolean,
    callName: string
}


class ReviewCreationForm extends React.Component<RouteComponentProps<Props>, LoginPageState> {

    constructor(props) {
        super(props);
        this.state = {
            grantApplicationId: this.props.match.params.id,
            isPanelChair: this.props.location.state.isChair,
            reviewer: this.props.location.state.account,
            grade: undefined,
            content: undefined,
            submissionError: "",
            submissionOk: false,
            acceptGrant: false,
            callName : this.props.location.state.callName,
        };
    }

    handleGradeChange = (event: any) => {
        this.setState(() => ({grade: event.target.value}))
    }

    handleContentChange = (event: any) => {
        this.setState(() => ({content: event.target.value}))
    }

    handleAcceptGrantChange = () => {
        this.setState(() => ({acceptGrant: !this.state.acceptGrant}))
    }

    submitReview = async (event: any) => {
        event.preventDefault();
        const form: ReviewDTO = {
            id: 0,
            note: this.state.grade,
            description: this.state.content,
            reviewerID: this.state.reviewer.accountId,
            grantApplicationId: this.state.grantApplicationId,
        }

        await ReviewsControllerApiFp().createReviewUsingPOST(form, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.reviewer.username + ':' + this.state.reviewer.password),
            }
        })().then(() => {
            this.setState({
                submissionOk: true
            })
        }).catch((errorMsg) => {
            switch (errorMsg.status) {
                case 409:
                    this.setState({
                        submissionError: "You already have written a review to this grant application."
                    });
                    break;
                case 400:
                    this.setState({
                        submissionError: "Please make sure you are filling all the fields."
                    })
                    break;
                default: {
                    console.log("Unexpected Server Error.")
                    break;
                }
            }
        })
    }

    submitFinalReview = async (event: any) => {
        event.preventDefault();
        const form: FinalReviewDTO = {
            id: 0,
            note: this.state.grade,
            description: this.state.content,
            reviewerID: this.state.reviewer.accountId,
            grantApplicationId: this.state.grantApplicationId,
            accepted: this.state.acceptGrant
        }

        await ReviewsControllerApiFp().createFinalReviewUsingPOST(form, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.reviewer.username + ':' + this.state.reviewer.password),
            }
        })().then(() => confirmAlert({
            title: 'Review Submission',
            message: 'Your review was submitted',
            buttons: [
                {
                    label: 'ok',
                    onClick: () => this.setState({submissionOk: true})
                },

            ]
        })).catch((errorMsg) => {
            switch (errorMsg.status) {
                case 409:
                    this.setState({
                        submissionError: "You already have written a review to this grant application."
                    });
                    break;
                case 400:
                    this.setState({
                        submissionError: "Please make sure you are filling all the fields."
                    })
                    break;
                default: {
                    console.log("Unexpected Server Error.")
                    break;
                }
            }
        })
    }

    render() {
        if (this.state.submissionOk) {
            return (<Redirect to={{
                pathname: "/grant_application/" + this.state.grantApplicationId + "/reviews",
                state: {account: this.state.reviewer , callName: this.state.callName}
            }}/>)
        } else {
            return (
                <div className="">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="card-heading text-center">
                                <h4 className="modal-title w-100 font-weight-bold">{this.state.isPanelChair ? "Final Review" : "New Review"}</h4>
                            </div>
                            <div className="modal-body mx-3">
                                <form onSubmit={this.state.isPanelChair ? this.submitFinalReview : this.submitReview}>
                                    <div className="md-form m-4">
                                        <label>Overall Grade</label>
                                        <input value={this.state.grade} className="form-control"
                                               onChange={this.handleGradeChange} type="number"
                                               min="0" max="20"
                                               placeholder="0"/>
                                        <small className="form-text text-muted float-right">Insert a value between 0 and
                                            20</small>
                                    </div>

                                    <div className="md-form m-4">
                                        <label> Personal Review</label>
                                        <textarea value={this.state.content} className="form-control"
                                                  onChange={this.handleContentChange}
                                                  placeholder="Review Detailed Description."/>
                                    </div>

                                    {
                                        this.state.isPanelChair ?
                                            <div className="md-form m-4 input-group-lg custom-switch">
                                                <input type="checkbox" className="custom-control-input"
                                                       id="customSwitch1" onChange={this.handleAcceptGrantChange}/>
                                                <label className="custom-control-label" htmlFor="customSwitch1">
                                                    Assign Grant
                                                </label>

                                            </div>

                                            :
                                            <div/>
                                    }

                                    <LoginError erroMsg={this.state.submissionError}/>
                                    <input type="submit" className="btn btn-primary float-right login-form-button"
                                           value="Submit"/>
                                </form>
                                <GoBackButton/>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

    }


}

function LoginError(props) {
    if (props.erroMsg !== "") {
        return (
            <div style={{color: 'red', margin: 15}}> {props.erroMsg}</div>
        )
    }
    return (<div/>)
}

export default ReviewCreationForm;
