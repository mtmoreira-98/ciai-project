import {FcGraduationCap, FcLock, FcMindMap} from "react-icons/fc";
import {Link} from "react-router-dom";
import React from "react";

const Body = () => {
    return (
        <div className="mt-4 rounded d-flex justify-content-center">
            <AnonymousComponent name="Opened Grant Calls" text="All opened grant calls" Icon={FcGraduationCap} pathname="/home/grant_calls"/>
            <AnonymousComponent name="All Grant Calls" text="All grant calls, include opened, closed and granted." Icon={FcMindMap} pathname="/home/all_grant_calls"/>
            <AnonymousComponent name="Closed Grant Calls" text="All closed grant calls" Icon={FcLock} pathname="/home/closed_grant_calls"/>
        </div>
    );
}

const AnonymousComponent = ({name, text, Icon, pathname}) => {
    return (
        <div className="image-block bg-light m-4 rounded shadow w-25 text-center">
            <Link to={{pathname: pathname}}>
                <div className="d-flex justify-content-center align-content-start m-4">
                    <Icon size={100}/>
                </div>
                <h3 className="text-muted ce">{name}</h3>
            </Link>
            <h5 className="m-5 text-success ce">{text}</h5>
        </div>
    );
}


export default Body;
